import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Color.dart';
import '/Getxcontroller/controllerphoneinfo.dart';

class Box_All extends StatelessWidget {
  const Box_All(
      {Key? key,
      required this.width,
      required this.hight,
      required this.widget,
      required this.text1})
      : super(key: key);
  final double width;
  final double hight;
  final Widget widget;
  final String text1;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: hight,
      child: Stack(
        children: [
          Align(
            child: Container(
              width: width,
              height: hight * 0.83,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.topLeft,
                      colors: [
                        theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                        theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2]
                      ])),
              child: Align(
                alignment: Alignment.topCenter,
                child: Text(
                  text1,
                  style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![1],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            left: 5,
            right: 5,
            top: 35,
            child: Container(
              //width: 200,
              height: hight * 0.83,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![1],
              ),
              child: widget,
            ),
          )
        ],
      ),
    );
  }
}

class Box_All_1 extends StatelessWidget {
  const Box_All_1(
      {Key? key,
      required this.width,
      required this.hight,
      required this.widget,
      required this.text1})
      : super(key: key);
  final double width;
  final double hight;
  final Widget widget;
  final String text1;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: hight,
      child: Stack(
        children: [
          Align(
            child: Container(
              width: width,
              height: hight * 0.83,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.topLeft,
                      colors: [
                        theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                        theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2]
                      ])),
              child: Align(
                alignment: Alignment.topCenter,
                child: Text(
                  text1,
                  style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![1],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            left: 5,
            right: 5,
            top: 15,
            child: Container(
              //width: 200,
              height: hight * 0.83,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![1],
              ),
              child: widget,
            ),
          )
        ],
      ),
    );
  }
}
