// ignore_for_file: file_names

import 'package:flutter/material.dart';

Map<String, List<Color>> theme = {
  "blue": [
/*     Colors.yellow[300]!,
    Colors.grey[800]!,
    Colors.deepOrange,
    const Color(0xFFF3B1B1) */
    Color(0xFF049BB6),
    Color(0xFFE7EEF1),
    Color(0xFF414249),
    Color(0xFFC1DAEB)
  ],
  "red": [
    const Color(0xFF720A0A),
    const Color(0xFF1B1717),
    const Color(0xFFE7E7E7),
    const Color(0xFFF3B1B1)
    /*   */
  ],
  'purple': [
    //Color(0xFF000000)
    const Color(0xFF3E035A),
    const Color(0xFFE6DADA),
    const Color(0xFFE96D6D),
    const Color(0xFFB330CE)
  ],
};
