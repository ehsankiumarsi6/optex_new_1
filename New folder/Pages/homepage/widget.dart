import 'dart:ui';

import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:liquid_progress_indicator_v2/liquid_progress_indicator.dart';
//import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import '/Getxcontroller/controllerOnOff.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Getxcontroller/controllershow.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';
import '/Widget/widget1.dart';
import 'package:telephony/telephony.dart';

import '../../Color.dart';

TextStyle textstyle_inqury = TextStyle(
    color: theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
    fontWeight: FontWeight.w600);
List<String> text_inqury = [
  'وضعیت',
  'برق شهر',
  'بلندگو',
  'ولتاژ باتری',
  'رله 1',
  'رله 2',
  'تعداد مخاطبین',
  'تعداد ریموت',
  'قدرت آنتن',
  'وضعیت شبکه',
  'زون 1',
  'زون 2',
  'زون 3',
  'زون 4'
];

class controlle_onoff extends StatelessWidget {
  const controlle_onoff({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ////////////////////////////////////////
        box1(),
        box2(),
      ],
    );
  }
}

class box2 extends StatelessWidget {
  const box2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: PhysicalModel(
          color: theme[
              Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
          elevation: 12,
          shadowColor: theme[
              Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![3],
          borderRadius: BorderRadius.circular(20),
          child: Container(
            //padding: EdgeInsets.all(3),
            width: 90,
            height: Get.height * 0.45,
            decoration: BoxDecoration(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![1],
                borderRadius: BorderRadius.all(Radius.circular(30))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Container(
                        width: 70,
                        height: 70,
                        child: LiquidCircularProgressIndicator(
                          value: Get.find<controllerphoneinfo>(tag: 'secend')
                                      .Charge
                                      .value >
                                  (Get.find<controllerphoneinfo>(tag: 'secend')
                                          .Chargemin
                                          .value *
                                      10)
                              ? (Get.find<controllerphoneinfo>(tag: 'secend')
                                          .Charge
                                          .value -
                                      (Get.find<controllerphoneinfo>(tag: 'secend')
                                              .Chargemin
                                              .value *
                                          10)) /
                                  ((Get.find<controllerphoneinfo>(tag: 'secend')
                                              .Chargemax
                                              .value *
                                          10) -
                                      (Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .Chargemin
                                              .value *
                                          10)) *
                                  100
                              : 0, // Defaults to 0.5.
                          valueColor: AlwaysStoppedAnimation(
                            theme[Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0],
                          ), // Defaults to the current Theme's accentColor.
                          backgroundColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![
                              1], // Defaults to the current Theme's backgroundColor.
                          borderColor: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2],
                          borderWidth: 2.0,
                          direction: Axis
                              .vertical, // The direction the liquid moves (Axis.vertical = bottom to top, Axis.horizontal = left to right). Defaults to Axis.vertical.
                          center: Icon(
                            Icons.monetization_on_outlined,
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                          ),
                        ),
                      ),
                    ),
                    Text(
                      'میزان اعتبار',
                      style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                      ),
                    ),
                    Obx(() {
                      return Text(
                        '${Get.find<controllerphoneinfo>(tag: 'secend').Charge.value}تومان',
                        style: TextStyle(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2]),
                      );
                    }),
                  ],
                ),
                Obx(() {
                  return Text(
                    '${Get.find<controllerphoneinfo>(tag: 'secend').Name.value == '' ? 'نام دستگاه' : Get.find<controllerphoneinfo>(tag: 'secend').Name.value}',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2]),
                  );
                }),
                Obx(() {
                  return Text(
                      Get.find<controllerphoneinfo>(tag: 'secend').id.value !=
                              ''
                          ? 'دستگاه:${Get.find<controllerphoneinfo>(tag: 'secend').id.value}'
                          : '',
                      style: TextStyle(
                          color: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2]));
                }),
                Container(
                  height: Get.height * 0.45 * 0.5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(
                        onTap: () {
                          TextEditingController TextCharge =
                              TextEditingController();
                          Get.defaultDialog(
                              backgroundColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![1],
                              title: 'شارژ اعتبار',
                              titleStyle: textstyle_inqury,
                              content: Column(
                                children: [
                                  Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: theme[
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .theme
                                                  .value]![0],
                                        ),
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.all(3),
                                      child: TextField(
                                          controller: TextCharge,
                                          style: textstyle_inqury,
                                          keyboardType: TextInputType.phone,
                                          decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText:
                                                  'کد شارژ اینجا وارد کنید...',
                                              hintStyle: textstyle_inqury))),
                                  InkWell(
                                    onTap: Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .fifteensecends
                                                .value >=
                                            15
                                        ? () async {
                                            Get.back();
                                            Get.defaultDialog(
                                                backgroundColor: theme[Get.find<
                                                            controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .theme
                                                    .value]![1],
                                                title: 'هشدار',
                                                titleStyle: textstyle_inqury,
                                                middleTextStyle:
                                                    textstyle_inqury,
                                                middleText:
                                                    'پیامک فرستاده شود؟',
                                                confirmTextColor: theme[
                                                    Get.find<controllerphoneinfo>(
                                                            tag: 'secend')
                                                        .theme
                                                        .value]![2],
                                                buttonColor: theme[Get.find<
                                                            controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .theme
                                                    .value]![0],
                                                textConfirm: 'بله',
                                                onConfirm: () {
                                                  final Telephony telephony =
                                                      Telephony.instance;
                                                  telephony.sendSms(
                                                    to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                                    message:
                                                        '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*60*${TextCharge.text}#',
                                                  );
                                                  Get.back();
                                                  Get.snackbar('اطلاعیه',
                                                      'پیامک فرستاده شد');
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .startfifteensecends();
                                                });
                                          }
                                        : () => Get.snackbar('خطا',
                                            'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![0],
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.all(10),
                                      child: Text(
                                        'انجام شارژ',
                                        style: textstyle_inqury,
                                      ),
                                    ),
                                  )
                                ],
                              ));
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          width: 97,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![0],
                          ),
                          child: Center(
                            child: Text(
                              'شارژ اعتبار',
                              style: TextStyle(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![2],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controlleronoff>(tag: 'secend')
                              .InquiryCharge
                              .value,
                          child: CircularProgressIndicator(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![0],
                          ),
                          replacement: Obx(() {
                            return InkWell(
                              onTap: Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .fifteensecends
                                          .value >=
                                      15
                                  ? () async {
                                      Get.defaultDialog(
                                          backgroundColor: theme[
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .theme
                                                  .value]![1],
                                          title: 'هشدار',
                                          titleStyle: textstyle_inqury,
                                          middleTextStyle: textstyle_inqury,
                                          middleText: 'پیامک فرستاده شود؟',
                                          confirmTextColor: theme[
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .theme
                                                  .value]![2],
                                          buttonColor: theme[
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .theme
                                                  .value]![0],
                                          textConfirm: 'بله',
                                          onConfirm: () {
                                            final Telephony telephony =
                                                Telephony.instance;

                                            if (Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .OpratorSim
                                                    .value ==
                                                'rl') {
                                              telephony.sendSms(
                                                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                                message:
                                                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*60*200*2*2*1#',
                                              );
                                            }
                                            if (Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .OpratorSim
                                                    .value ==
                                                'ir') {
                                              telephony.sendSms(
                                                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                                message:
                                                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*60*141*1#',
                                              );
                                            }
                                            if (Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .OpratorSim
                                                    .value ==
                                                'ha') {
                                              telephony.sendSms(
                                                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                                message:
                                                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*60*140*11#',
                                              );
                                            }
                                            Get.back();
                                            Get.find<controlleronoff>(
                                                    tag: 'secend')
                                                .InquiryCharge
                                                .value = true;
                                            Get.snackbar(
                                                'اطلاعیه', 'پیامک فرستاده شد');
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .startfifteensecends();
                                            telephony.listenIncomingSms(
                                                onNewMessage:
                                                    (SmsMessage message) {
                                                  /*  if (message.address == "+98${phone.value}" &&
                                message.body!.split('bar').length > 0) {
                                              Get.snackbar('اطلاعیه', 'مقدار شارژ دریافت شد');
                                              Get.find<controlleronoff>(tag: 'secend')
                                  .InquiryCharge
                                  .value = false;
                                              Charge.value = int.parse(message.body!.split(' ')[1]);
                                              updatePhone();
                                            } */
                                                  /*  if (message.address == "+98${phone.value}" &&
                                message.body!.split('شما').length > 0) { */
                                                  if (Get.find<controlleronoff>(
                                                              tag: 'secend')
                                                          .InquiryCharge
                                                          .value ==
                                                      true) {
                                                    Get.snackbar('اطلاعیه',
                                                        'مقدار شارژ دریافت شد');
                                                    Get.find<controlleronoff>(
                                                            tag: 'secend')
                                                        .InquiryCharge
                                                        .value = false;

                                                    Get.find<controllerphoneinfo>(
                                                                tag: 'secend')
                                                            .Charge
                                                            .value =
                                                        int.parse(message.body!
                                                            .replaceAll(
                                                                new RegExp(
                                                                    r'[^0-9]'),
                                                                ''));
                                                    Get.snackbar('اطلاعیه',
                                                        '${Get.find<controllerphoneinfo>(tag: 'secend').Charge.value}');
                                                    Get.find<controllerphoneinfo>(
                                                            tag: 'secend')
                                                        .updatePhone();
                                                  }
                                                },
                                                listenInBackground: false);
                                          });
                                    }
                                  : () => Get.snackbar('خطا',
                                      'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 8),
                                width: 97,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![0],
                                ),
                                child: Center(
                                  child: Text(
                                    'استعلام شارژ',
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[2]
                              .value,
                          child: Obx(() {
                            return InkWell(
                              onTap: Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .fifteensecends
                                          .value >=
                                      15
                                  ? () async {
                                      Get.defaultDialog(
                                          backgroundColor: theme[
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .theme
                                                  .value]![1],
                                          title: 'هشدار',
                                          titleStyle: textstyle_inqury,
                                          middleTextStyle: textstyle_inqury,
                                          middleText: 'پیامک فرستاده شود؟',
                                          confirmTextColor: theme[
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .theme
                                                  .value]![2],
                                          buttonColor: theme[
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .theme
                                                  .value]![0],
                                          textConfirm: 'بله',
                                          onConfirm: () {
                                            final Telephony telephony =
                                                Telephony.instance;
                                            telephony.sendSms(
                                              to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                              message:
                                                  '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*62#',
                                            );
                                            Get.back();
                                            Get.snackbar(
                                                'اطلاعیه', 'پیامک ارسال شد');
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .startfifteensecends();
                                          });
                                    }
                                  : () => Get.snackbar('خطا',
                                      'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 8),
                                width: 97,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![0],
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.hearing,
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    ),
                                    Text(
                                      'شنود',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                        );
                      }),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}

class box1 extends StatefulWidget {
  const box1({
    Key? key,
  }) : super(key: key);

  @override
  State<box1> createState() => _box1State();
}

class _box1State extends State<box1> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.435,
      width: Get.width * 0.4,
      // ignore: sort_child_properties_last
      child: Center(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              children: [
                Obx(() {
                  return InkWell(
                      onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                                  .fifteensecends
                                  .value >=
                              15
                          ? () async {
                              Get.defaultDialog(
                                  backgroundColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![1],
                                  title: 'هشدار',
                                  titleStyle: textstyle_inqury,
                                  middleTextStyle: textstyle_inqury,
                                  middleText: 'پیامک فرستاده شود؟',
                                  confirmTextColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![2],
                                  buttonColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![0],
                                  textConfirm: 'بله',
                                  onConfirm: () async {
                                    Get.back();
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value = 'off';
                                    final Telephony telephony =
                                        Telephony.instance;
                                    telephony.sendSms(
                                      to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                      message:
                                          '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*10#',
                                    );
                                    await Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .updatePhone();
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .startfifteensecends();
                                    Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                                  });
                            }
                          : () => Get.snackbar(
                              'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                      child: Container(
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            color: Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value ==
                                    'off'
                                ? theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0]
                                : theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![1],
                            border: Border.all(color: Color(0xFFFFFFFF)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        height: (Get.height * 0.3) - 5,
                        width: (Get.width * 0.4) / 2.1,
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Column(
                            children: [
                              Icon(
                                Icons.lock_open,
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![2],
                              ),
                              Text(
                                'غیر فعال',
                                style: TextStyle(
                                    color: theme[Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .theme
                                        .value]![2],
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                      ));
                }),
                Obx(() {
                  return InkWell(
                      onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                                  .fifteensecends
                                  .value >=
                              15
                          ? () async {
                              Get.defaultDialog(
                                  backgroundColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![1],
                                  title: 'هشدار',
                                  titleStyle: textstyle_inqury,
                                  middleTextStyle: textstyle_inqury,
                                  middleText: 'پیامک فرستاده شود؟',
                                  confirmTextColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![2],
                                  buttonColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![0],
                                  textConfirm: 'بله',
                                  onConfirm: () async {
                                    Get.back();
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value = 'on';
                                    final Telephony telephony =
                                        Telephony.instance;
                                    telephony.sendSms(
                                      to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                      message:
                                          '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*11#',
                                    );
                                    await Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .updatePhone();
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .startfifteensecends();
                                    Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                                  });
                            }
                          : () => Get.snackbar(
                              'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                      child: Container(
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                            color: Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value ==
                                    'on'
                                ? theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0]
                                : theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![1],
                            border: Border.all(color: Color(0xFFFFFFFF)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(25))),
                        height: (Get.height * 0.3) - 5,
                        width: (Get.width * 0.4) / 2.1,
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'فعال',
                                style: TextStyle(
                                    color: theme[Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .theme
                                        .value]![2],
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400),
                              ),
                              Icon(
                                Icons.lock,
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![2],
                              ),
                            ],
                          ),
                        ),
                      ));
                })
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 3),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Obx(() {
                  return Visibility(
                    visible:
                        Get.find<controllershow>(tag: 'secend').showf[1].value,
                    child: Obx(() {
                      return InkWell(
                          onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                                      .fifteensecends
                                      .value >=
                                  15
                              ? () async {
                                  Get.defaultDialog(
                                      backgroundColor: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![1],
                                      title: 'هشدار',
                                      titleStyle: textstyle_inqury,
                                      middleTextStyle: textstyle_inqury,
                                      middleText: 'پیامک فرستاده شود؟',
                                      confirmTextColor: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                      buttonColor: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![0],
                                      textConfirm: 'بله',
                                      onConfirm: () async {
                                        Get.back();
                                        Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .OnPhones
                                            .value = 'halfon';
                                        final Telephony telephony =
                                            Telephony.instance;
                                        telephony.sendSms(
                                          to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                          message:
                                              '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*13#',
                                        );
                                        await Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .updatePhone();
                                        Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .startfifteensecends();
                                        Get.snackbar(
                                            'اطلاعیه', 'پیامک فرستاده شد');
                                      });
                                }
                              : () => Get.snackbar('خطا',
                                  'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                          child: Obx(() {
                            return Container(
                              padding: EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                  color: Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .OnPhones
                                              .value ==
                                          'halfon'
                                      ? theme[Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![0]
                                      : theme[Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![1],
                                  border: Border.all(color: Color(0xFFFFFFFF)),
                                  borderRadius: BorderRadius.circular(15)),
                              width: (Get.width * 0.4) / 2.1,
                              height: (Get.width * 0.45) / 2.1,
                              child: Column(children: [
                                Text(
                                  'نیمه فعال',
                                  style: TextStyle(
                                    color: theme[Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .theme
                                        .value]![2],
                                  ),
                                ),
                                Icon(
                                  Icons.shield,
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![2],
                                )
                              ]),
                            );
                          }));
                    }),
                  );
                }),
                Obx(() {
                  return Visibility(
                    visible:
                        Get.find<controllershow>(tag: 'secend').showf[0].value,
                    child: Obx(() {
                      return InkWell(
                          onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                                      .fifteensecends
                                      .value >=
                                  15
                              ? () async {
                                  Get.defaultDialog(
                                      backgroundColor: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![1],
                                      title: 'هشدار',
                                      titleStyle: textstyle_inqury,
                                      middleTextStyle: textstyle_inqury,
                                      middleText: 'پیامک فرستاده شود؟',
                                      confirmTextColor: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                      buttonColor: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![0],
                                      textConfirm: 'بله',
                                      onConfirm: () async {
                                        Get.back();
                                        Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .OnPhones
                                            .value = 'silent';
                                        final Telephony telephony =
                                            Telephony.instance;
                                        telephony.sendSms(
                                          to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                          message:
                                              '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*12#',
                                        );
                                        await Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .updatePhone();
                                        Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .startfifteensecends();
                                        Get.snackbar(
                                            'اطلاعیه', 'پیامک فرستاده شد');
                                      });
                                }
                              : () => Get.snackbar('خطا',
                                  'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                          child: Obx(() {
                            return Container(
                              padding: EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                  color: Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .OnPhones
                                              .value ==
                                          'silent'
                                      ? theme[Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![0]
                                      : theme[Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![1],
                                  border: Border.all(color: Color(0xFFFFFFFF)),
                                  borderRadius: BorderRadius.circular(15)),
                              width: (Get.width * 0.4) / 2.1,
                              height: (Get.width * 0.45) / 2.1,
                              child: Column(children: [
                                Text(
                                  'سایلنت',
                                  style: TextStyle(
                                    color: theme[Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .theme
                                        .value]![2],
                                  ),
                                ),
                                Icon(
                                  Icons.music_off_outlined,
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![2],
                                )
                              ]),
                            );
                          }));
                    }),
                  );
                })
              ]),
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
          color: theme[
              Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
          borderRadius: BorderRadius.all(Radius.circular(30))),
    );
  }
}

class bottom_sheet extends StatelessWidget {
  const bottom_sheet({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: DraggableScrollableSheet(
        maxChildSize: 0.9,
        initialChildSize: 0.065,
        minChildSize: 0.065,
        builder: (context, scrollController) {
          return Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: Get.width * 0.5,
                  child: Center(
                    child: ListView.builder(
                      controller: scrollController,
                      itemCount: 6,
                      itemBuilder: (BuildContext context, int index) {
                        if (index == 0) {
                          return Center(
                            child: Text(
                              'نمایش زون ها',
                              style: TextStyle(
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![2],
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold),
                            ),
                          );
                        } else if (index == 1) {
                          return Obx(() {
                            return Visibility(
                              visible: Get.find<controlleronoff>(tag: 'secend')
                                  .Inquiryzoon
                                  .value,
                              child: Container(
                                  width: 10,
                                  height: 10,
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![0],
                                    ),
                                  )),
                              replacement: Obx(() {
                                return InkWell(
                                  onTap: Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .fifteensecends
                                              .value >=
                                          15
                                      ? () async {
                                          Get.defaultDialog(
                                              backgroundColor: theme[
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![1],
                                              title: 'هشدار',
                                              titleStyle: textstyle_inqury,
                                              middleTextStyle: textstyle_inqury,
                                              middleText: 'پیامک فرستاده شود؟',
                                              confirmTextColor: theme[
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![2],
                                              buttonColor: theme[
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![0],
                                              textConfirm: 'بله',
                                              onConfirm: () {
                                                List<String> type = [
                                                  'N',
                                                  'N',
                                                  'H',
                                                  'D',
                                                  'G',
                                                  'S'
                                                ];
                                                List<
                                                    String> copyzone = Get.find<
                                                            controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .Zone
                                                    .value
                                                    .split(' ');
                                                for (var i = 1;
                                                    i <
                                                        '*Z1N*Z2N*Z3N*Z4N#'
                                                            .split('*Z')
                                                            .length;
                                                    i++) {
                                                  for (var j = 1;
                                                      j < type.length;
                                                      j++) {
                                                    if ('*Z1N*Z2N*Z3N*Z4N#'
                                                                .split('*Z')[i]
                                                            [1] ==
                                                        type[j]) {
                                                      //   j == 1 ? j = 0 : print('hello');

                                                      copyzone[i - 1] = '$j';
                                                      /*  print(
                                            '*Z1N*Z2H*Z3D*Z4G#'.split('*Z')[i][1]);
                                        print(type[j]); */
                                                      // print(j);
                                                    }
                                                    print(j);
                                                  }
                                                }
                                                Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .Zone
                                                    .value = '';
                                                for (var i = 0;
                                                    i < copyzone.length;
                                                    i++) {
                                                  Get.find<controllerphoneinfo>(
                                                              tag: 'secend')
                                                          .Zone
                                                          .value =
                                                      Get.find<controllerphoneinfo>(
                                                                  tag: 'secend')
                                                              .Zone
                                                              .value +
                                                          copyzone[i] +
                                                          (i == 14 ? '' : ' ');
                                                }
                                                print(Get.find<
                                                            controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .Zone);
                                                Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .updatePhone();
                                                List<String> typezoon = [
                                                  'Normaly close',
                                                  'Normaly Open',
                                                  '24 ساعت',
                                                  'دینگ دانگ',
                                                  'حفاظت',
                                                  'جاسوسی',
                                                  'آبی',
                                                  '24 ساعت جاسوسی آبی'
                                                ];

                                                for (var i = 0;
                                                    i <
                                                        (Get.find<controllerphoneinfo>(
                                                                    tag:
                                                                        'secend')
                                                                .Zone
                                                                .value
                                                                .split(' ')
                                                                .length -
                                                            2);
                                                    i++) {
                                                  Get.find<controllerphoneinfo>(
                                                              tag: 'secend')
                                                          .zonesdropdown[
                                                      i] = typezoon[int.parse(Get
                                                          .find<controllerphoneinfo>(
                                                              tag: 'secend')
                                                      .Zone
                                                      .value
                                                      .split(' ')[i])];
                                                }
                                                final Telephony telephony =
                                                    Telephony.instance;
                                                //  String copyMassege = '0,1,1110,0,1,30,1,1,99,0';
                                                telephony.sendSms(
                                                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                                  message: // '*Z1N*Z2H*Z3D*Z4G#',

                                                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*92#',
                                                );
                                                Get.back();

                                                Get.find<controlleronoff>(
                                                        tag: 'secend')
                                                    .Inquiryzoon
                                                    .value = true;

                                                Get.snackbar('اطلاعیه',
                                                    'پیامک ارسال شد');
                                                Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .startfifteensecends();
                                                telephony.listenIncomingSms(
                                                    onNewMessage:
                                                        (SmsMessage message) {
                                                      // Handle message
                                                      if (message.address ==
                                                              "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}" &&
                                                          message.body!
                                                                  .split('*Z')
                                                                  .length >
                                                              0) {
                                                        Get.find<controlleronoff>(
                                                                tag: 'secend')
                                                            .Inquiryzoon
                                                            .value = false;
                                                        Get.snackbar('اطلاعیه',
                                                            'پیامک دریافت شد');
                                                        List<String> type = [
                                                          'N',
                                                          'N',
                                                          'H',
                                                          'D',
                                                          'G',
                                                          'S'
                                                        ];
                                                        List<String> copyzone =
                                                            Get.find<controllerphoneinfo>(
                                                                    tag:
                                                                        'secend')
                                                                .Zone
                                                                .value
                                                                .split(' ');
                                                        for (var i = 1;
                                                            i <
                                                                message.body!
                                                                    .split('*Z')
                                                                    .length;
                                                            i++) {
                                                          for (var j = 1;
                                                              j < type.length;
                                                              j++) {
                                                            if (message.body!
                                                                        .split(
                                                                            '*Z')[
                                                                    i][1] ==
                                                                type[j]) {
                                                              // j == 1 ? j = 0 : print('hello');
                                                              copyzone[i - 1] =
                                                                  '$j';
                                                            }
                                                          }
                                                        }
                                                        Get.find<controllerphoneinfo>(
                                                                tag: 'secend')
                                                            .Zone
                                                            .value = '';
                                                        for (var i = 0;
                                                            i < copyzone.length;
                                                            i++) {
                                                          Get.find<controllerphoneinfo>(
                                                                  tag: 'secend')
                                                              .Zone
                                                              .value = Get.find<
                                                                          controllerphoneinfo>(
                                                                      tag:
                                                                          'secend')
                                                                  .Zone
                                                                  .value +
                                                              copyzone[i] +
                                                              (i == 14
                                                                  ? ''
                                                                  : ' ');

                                                          print(i);
                                                        }
                                                        Get.find<controllerphoneinfo>(
                                                                tag: 'secend')
                                                            .updatePhone();
                                                        List<String> typezoon =
                                                            [
                                                          'Normaly close',
                                                          'Normaly Open',
                                                          '24 ساعت',
                                                          'دینگ دانگ',
                                                          'حفاظت',
                                                          'جاسوسی',
                                                          'آبی',
                                                          '24 ساعت جاسوسی آبی'
                                                        ];

                                                        for (var i = 0;
                                                            i <
                                                                (Get.find<controllerphoneinfo>(
                                                                            tag:
                                                                                'secend')
                                                                        .Zone
                                                                        .value
                                                                        .split(
                                                                            ' ')
                                                                        .length -
                                                                    2);
                                                            i++) {
                                                          Get.find<controllerphoneinfo>(
                                                                      tag: 'secend')
                                                                  .zonesdropdown[
                                                              i] = typezoon[int.parse(Get.find<
                                                                      controllerphoneinfo>(
                                                                  tag: 'secend')
                                                              .Zone
                                                              .value
                                                              .split(' ')[i])];
                                                        }
                                                      }
                                                    },
                                                    listenInBackground: false);
                                              });
                                        }
                                      : () => Get.snackbar('خطا',
                                          'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                                  child: Container(
                                    width: Get.width * 0.35,
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    decoration: BoxDecoration(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![0],
                                        borderRadius:
                                            BorderRadius.circular(45)),
                                    child: Center(
                                        child: Text('استعلام زون ها',
                                            style: TextStyle(
                                                color: theme[Get.find<
                                                            controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .theme
                                                    .value]![2],
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600))),
                                  ),
                                );
                              }),
                            );
                          });
                        } else {
                          return Column(
                            children: [
                              Container(
                                width: Get.width * 0.5,
                                child: Center(
                                  child: Box_All(
                                    hight: 100,
                                    width: Get.width * 0.45,
                                    widget: Center(
                                      child: Obx(() {
                                        return DropdownButton(
                                          value: Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .zonesdropdown[index],
                                          onChanged: Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .fifteensecends
                                                      .value >=
                                                  15
                                              ? (String? newValue) async {
                                                  Get.defaultDialog(
                                                      backgroundColor: theme[
                                                          Get.find<controllerphoneinfo>(
                                                                  tag: 'secend')
                                                              .theme
                                                              .value]![1],
                                                      title: 'هشدار',
                                                      titleStyle:
                                                          textstyle_inqury,
                                                      middleTextStyle:
                                                          textstyle_inqury,
                                                      middleText:
                                                          'پیامک فرستاده شود؟',
                                                      confirmTextColor: theme[
                                                          Get.find<controllerphoneinfo>(
                                                                  tag: 'secend')
                                                              .theme
                                                              .value]![2],
                                                      buttonColor: theme[
                                                          Get.find<controllerphoneinfo>(
                                                                  tag: 'secend')
                                                              .theme
                                                              .value]![0],
                                                      textConfirm: 'بله',
                                                      onConfirm: () {
                                                        Get.find<controllerphoneinfo>(
                                                                    tag: 'secend')
                                                                .zonesdropdown[
                                                            index] = newValue!;

                                                        List<String> type = [
                                                          'Normaly close',
                                                          'Normaly Open',
                                                          '24 ساعت',
                                                          'دینگ دانگ',
                                                          'حفاظت',
                                                          'جاسوسی',
                                                          'آبی',
                                                          '24 ساعت جاسوسی آبی'
                                                        ];

                                                        for (var i = 0;
                                                            i < type.length;
                                                            i++) {
                                                          if (Get.find<controllerphoneinfo>(
                                                                          tag:
                                                                              'secend')
                                                                      .zonesdropdown[
                                                                  index] ==
                                                              type[i]) {
                                                            List<String>
                                                                copyzone =
                                                                Get.find<controllerphoneinfo>(
                                                                        tag:
                                                                            'secend')
                                                                    .Zone
                                                                    .value
                                                                    .split(' ');
                                                            copyzone[index] =
                                                                '$i';
                                                            print(copyzone);
                                                            Get.find<controllerphoneinfo>(
                                                                    tag:
                                                                        'secend')
                                                                .Zone
                                                                .value = '';
                                                            for (var i = 0;
                                                                i <
                                                                    copyzone
                                                                        .length;
                                                                i++) {
                                                              Get.find<controllerphoneinfo>(
                                                                      tag:
                                                                          'secend')
                                                                  .Zone
                                                                  .value = Get.find<
                                                                              controllerphoneinfo>(
                                                                          tag:
                                                                              'secend')
                                                                      .Zone
                                                                      .value +
                                                                  copyzone[i] +
                                                                  (i == 14
                                                                      ? ''
                                                                      : ' ');

                                                              print(i);
                                                            }
                                                            i == 1
                                                                ? i = 5
                                                                : null;
                                                            i == 0
                                                                ? i = 1
                                                                : null;
                                                            final Telephony
                                                                telephony =
                                                                Telephony
                                                                    .instance;
                                                            telephony.sendSms(
                                                              to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                                              message:
                                                                  '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*42*${index + 1}*${i}#',
                                                            );
                                                            print(
                                                                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*42*${index + 1}*${i}#');
                                                            Get.find<controllerphoneinfo>(
                                                                    tag:
                                                                        'secend')
                                                                .updatePhone();
                                                            print(Get.find<
                                                                        controllerphoneinfo>(
                                                                    tag:
                                                                        'secend')
                                                                .Zone
                                                                .value);
                                                          }
                                                        }
                                                        Get.back();

                                                        Get.snackbar('اطلاعیه',
                                                            'پیامک ارسال شد');
                                                        Get.find<controllerphoneinfo>(
                                                                tag: 'secend')
                                                            .startfifteensecends();
                                                      });
                                                }
                                              : (String? newValue) => Get.snackbar(
                                                  'خطا',
                                                  'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                                          items: <String>[
                                            'Normaly close',
                                            'Normaly Open',
                                            '24 ساعت',
                                            'دینگ دانگ',
                                            'حفاظت',
                                            'جاسوسی',
                                            'آبی',
                                            '24 ساعت جاسوسی آبی'
                                          ].map<DropdownMenuItem<String>>(
                                              (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              alignment:
                                                  AlignmentDirectional.center,
                                              child: Text(
                                                value,
                                                style: TextStyle(
                                                  color: theme[Get.find<
                                                              controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![2],
                                                ),
                                                textDirection:
                                                    TextDirection.rtl,
                                                textAlign: TextAlign.center,
                                              ),
                                            );
                                          }).toList(),
                                        );
                                      }),
                                    ),
                                    text1: 'زون${index - 1}',
                                  ),
                                ),
                              ),
                              Obx(() {
                                return InkWell(
                                  onTap: Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .fifteensecends
                                              .value >=
                                          15
                                      ? () async {
                                          Get.defaultDialog(
                                              backgroundColor: theme[
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![1],
                                              title: 'هشدار',
                                              titleStyle: textstyle_inqury,
                                              middleTextStyle: textstyle_inqury,
                                              middleText: 'پیامک فرستاده شود؟',
                                              confirmTextColor: theme[
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![2],
                                              buttonColor: theme[
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![0],
                                              textConfirm: 'بله',
                                              onConfirm: () async {
                                                final Telephony telephony =
                                                    Telephony.instance;
                                                telephony.sendSms(
                                                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                                  message:
                                                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*42*${index+1}*9#',
                                                );
                                                Get.back();

                                                Get.snackbar('اطلاعیه',
                                                    'پیامک فرستاده شد');
                                                Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .startfifteensecends();
                                              });
                                        }
                                      : () => Get.snackbar('خطا',
                                          'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                                  child: Icon(
                                    Icons.delete,
                                    color: theme[Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .theme
                                        .value]![2],
                                  ),
                                );
                              })
                            ],
                          );
                        }
                      },
                    ),
                  ),
                ),
                Container(
                  width: 5,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [
                        theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                        theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1]
                      ])),
                ),
                Container(
                  width: Get.width * 0.4,
                  child: ListView(
                    controller: scrollController,
                    children: [
                      Text(
                        'نمایش وضعیت ',
                        style: TextStyle(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      inqury_all(),
                      Container(
                        width: Get.width * 0.4,
                        margin:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 2),
                        child: Center(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Obx(() {
                              return Text(
                                  '${Get.find<controllerphoneinfo>(tag: 'secend').OnPhones == 'on' ? 'فعال' : Get.find<controllerphoneinfo>(tag: 'secend').OnPhones == 'off' ? 'غیر فعال' : Get.find<controllerphoneinfo>(tag: 'secend').OnPhones == 'silent' ? 'سایلنت' : 'نیمه فعال'}',
                                  style: TextStyle(
                                    color: theme[Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .theme
                                        .value]![2],
                                  ));
                            }),
                            Text('وضعیت',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![2],
                                )),
                          ],
                        )),
                      ),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[3]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').VoltAc.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('برق شهر',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[4]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Speaker.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('بلندگو',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[5]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Voltbatry.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('ولتاژباتری',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[6]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').StatusRelleh.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('رله 1',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[6]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text('',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('رله2',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[7]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').CountConuntect.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('تعداد مخاطبین',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[8]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Countremote.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('تعداد ریموت',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[9]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Anten.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('قدرت آنتن',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[10]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').StatusNetwork.value}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('وضعیت شبکه',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[11]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Statuszoons.value[0] == '1' ? 'بسته' : 'باز'}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('زون 1',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[12]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Statuszoons.value[1] == '1' ? 'بسته' : 'باز'}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('زون 2',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[13]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Statuszoons.value[2] == '1' ? 'بسته' : 'باز'}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('زون 3',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      }),
                      Obx(() {
                        return Visibility(
                          visible: Get.find<controllershow>(tag: 'secend')
                              .showf[14]
                              .value,
                          child: Container(
                            width: Get.width * 0.4,
                            margin: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Center(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Obx(() {
                                  return Text(
                                      '${Get.find<controllerphoneinfo>(tag: 'secend').Statuszoons.value[3] == '1' ? 'بسته' : 'باز'}',
                                      style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2],
                                      ));
                                }),
                                Text('زون 4',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                      color: theme[
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .theme
                                              .value]![2],
                                    )),
                              ],
                            )),
                          ),
                        );
                      })
                    ],
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],

              /// To set a shadow behind the parent container
              boxShadow: [
                BoxShadow(
                  color: Colors.white,
                  offset: Offset(0.0, -2.0),
                  blurRadius: 4.0,
                ),
              ],

              /// To set radius of top left and top right
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(24.0),
                topRight: Radius.circular(24.0),
              ),
            ),
          );
        },
      ),
    );
  }
}

class inqury_all extends StatelessWidget {
  const inqury_all({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Visibility(
        visible: Get.find<controlleronoff>(tag: 'secend').InquiryAll.value,
        child: Container(
            width: 10,
            height: 10,
            child: Center(
              child: CircularProgressIndicator(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
              ),
            )),
        replacement: Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () async {
                    Get.defaultDialog(
                        backgroundColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![1],
                        title: 'هشدار',
                        titleStyle: textstyle_inqury,
                        middleTextStyle: textstyle_inqury,
                        middleText: 'پیامک فرستاده شود؟',
                        confirmTextColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        buttonColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0],
                        textConfirm: 'بله',
                        onConfirm: () {
                          Get.back();
                          final Telephony telephony = Telephony.instance;
                          //  String copyMassege = '0,1,1110,0,1,30,1,1,99,0';
                          telephony.sendSms(
                            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                            message:
                                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*90#',
                          );

                          Get.find<controlleronoff>(tag: 'secend')
                              .InquiryAll
                              .value = true;
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .startfifteensecends();
                          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');

                          telephony.listenIncomingSms(
                              onNewMessage: (SmsMessage message) {
                                // Handle message
                                if (message.address ==
                                        "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}" &&
                                    message.body!.split(',').length > 4) {
                                  Get.find<controlleronoff>(tag: 'secend')
                                      .InquiryAll
                                      .value = false;
                                  List<String> resivesms =
                                      message.body!.split(',');
                                  Get.snackbar('${resivesms}اطلاعیه',
                                      'پیامک دریافت  شد');

                                  //1
                                  if (resivesms[0] == '0') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value = 'off';
                                  }
                                  if (resivesms[0] == '4') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value = 'on';
                                  }
                                  if (resivesms[0] == '3') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value = 'silent';
                                  }
                                  if (resivesms[0] == '2') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .OnPhones
                                        .value = 'halfon';
                                  }
                                  //2
                                  if (resivesms[1] == '1') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .VoltAc
                                        .value = 'روشن';
                                  } else {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .VoltAc
                                        .value = 'خاموش';
                                  }
                                  //3
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .Statuszoons
                                      .value = resivesms[2];
                                  //4
                                  if (resivesms[3] == '1') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .Speaker
                                        .value = 'روشن';
                                  } else {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .Speaker
                                        .value = 'خاموش';
                                  }
                                  //5
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .Countremote
                                      .value = resivesms[4];
                                  //6
                                  // String copyAnten='${(int.parse(resivesms[5]) / 31) * 100}'[0];
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                          .Anten
                                          .value =
                                      '${(int.parse(resivesms[5]) / 31) * 100}'[
                                              0] +
                                          '${(int.parse(resivesms[5]) / 31) * 100}'[
                                              1];
                                  //'${(int.parse(resivesms[5]) / 31) * 100}'[2] ;

                                  // '${(int.parse(resivesms[5]) / 31) * 100}'[3];
                                  // (resivesms[5]);

                                  //7
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .CountConuntect
                                      .value = resivesms[6];
                                  //8
                                  if (resivesms[7] == '1') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .StatusNetwork
                                        .value = 'روشن';
                                  } else {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .StatusNetwork
                                        .value = 'خاموش';
                                  }
                                  //9
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .Voltbatry
                                      .value = resivesms[8];

                                  //10
                                  if (resivesms[9][0] == '1') {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .StatusRelleh
                                        .value = 'روشن';
                                  } else {
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .StatusRelleh
                                        .value = 'خاموش';
                                  }

                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .updatePhone();
                                }
                              },
                              listenInBackground: false);
                        });
                  }
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Container(
              width: Get.width * 0.35,
              margin: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0],
                  borderRadius: BorderRadius.circular(45)),
              child: Center(
                  child: Text('استعلام دستگاه',
                      style: TextStyle(
                          color: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2],
                          fontSize: 18,
                          fontWeight: FontWeight.w600))),
            ),
          );
        }),
      );
    });
  }
}

class inqury extends StatelessWidget {
  const inqury({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.4,
      child: SingleChildScrollView(
          child: Column(
        children: [
          Text(
            'نمایش  وضعیت',
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![2],
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
          Container(
            width: Get.width * 0.3,
            decoration: BoxDecoration(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                borderRadius: BorderRadius.circular(35)),
            child: Center(
              child: Text(
                'استعلام دستگاه',
                style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
            ),
          ),

          ///
          ///
          ///
          ///
          ///
          Row(
            children: [
              Text(
                'وضعیت',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'برق شهر',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'بلندگو',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'ولتاژ باطری',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'رله1',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'رله2',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'تعداد مخاطبین',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'تعداد ریموت',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'قدرت آنتن',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'وضعیت شبکه',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'زون1',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'زون2',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'زون3',
                style: textstyle_inqury,
              )
            ],
          ),
          Row(
            children: [
              Text(
                'زون4',
                style: textstyle_inqury,
              )
            ],
          ),
        ],
      )),
    );
  }
}

class zoon extends StatelessWidget {
  const zoon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: Get.width * 0.5,
        child: SingleChildScrollView(
            child: Column(
          children: [
            Text(
              'نمایش زون ها',
              style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            Container(
              width: Get.width * 0.3,
              decoration: BoxDecoration(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0],
                  borderRadius: BorderRadius.circular(35)),
              child: Center(
                child: Text(
                  'استعلام زون ها',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                      fontWeight: FontWeight.bold,
                      fontSize: 17),
                ),
              ),
            ),
          ],
        )));
  }
}
