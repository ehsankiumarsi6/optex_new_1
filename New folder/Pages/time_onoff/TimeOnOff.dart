// ignore_for_file: file_names

import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:telephony/telephony.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';

class TimeOnOff extends StatefulWidget {
  const TimeOnOff({Key? key}) : super(key: key);

  @override
  State<TimeOnOff> createState() => _TimeOnOffState();
}

class _TimeOnOffState extends State<TimeOnOff> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'زمان بندی دستگاه',
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 20,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () async {
              var prefs = await SharedPreferences.getInstance();
              int minute = prefs.getInt('minute1') ?? 10;
              int hour = prefs.getInt('hour1') ?? 12;
              final TimeOfDay? newTime = await showTimePicker(
                context: context,
                initialTime: TimeOfDay(hour: hour, minute: minute),
              );
              if ((newTime) != null) {
                prefs.setInt('minute1', newTime.minute);
                prefs.setInt('hour1', newTime.hour);
                print(newTime.minute);
                print(newTime.hour);
                await AndroidAlarmManager.cancel(11);

                await AndroidAlarmManager.oneShotAt(
                    DateTime(DateTime.now().year, DateTime.now().month,
                        DateTime.now().year, newTime.hour, newTime.minute),
                    11, () async {
                  DateTime now = DateTime.now();
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  String phone1 = prefs.getString('phone1') ?? '0992';
                  String password = prefs.getString('password_1') ?? '0000';
                  print(
                      'TimeIs${now.hour}:${now.minute}:${now.second}:${now.millisecond}==is Alarm');

                  print('10');
                  final Telephony telephony = Telephony.instance;
                  telephony.sendSms(
                    to: "${phone1}",
                    message: '*${password}*10#',
                  );
                });
              }
            },
            child: Text(
              'تنظیم ساعت فعالسازی',
              style: textstyle_inqury,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 20,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () async {
              var prefs = await SharedPreferences.getInstance();
              int minute_1 = prefs.getInt('minute_1') ?? 10;
              int hour_1 = prefs.getInt('hour_1') ?? 12;
              final TimeOfDay? newTime = await showTimePicker(
                context: context,
                initialTime: TimeOfDay(hour: hour_1, minute: minute_1),
              );
              if ((newTime) != null) {
                prefs.setInt('minute_1', newTime.minute);
                prefs.setInt('hour_1', newTime.hour);
                print(newTime.minute);
                print(newTime.hour);
                await AndroidAlarmManager.cancel(10);
                await AndroidAlarmManager.oneShotAt(
                    DateTime(DateTime.now().year, DateTime.now().month,
                        DateTime.now().year, newTime.hour, newTime.minute),
                    10, () async {
                  DateTime now = DateTime.now();
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  String phone1 = prefs.getString('phone1') ?? '0992';
                  String password = prefs.getString('password_1') ?? '0000';
                  print(
                      'TimeIs${now.hour}:${now.minute}:${now.second}:${now.millisecond}==is Alarm');

                  print('10');
                  final Telephony telephony = Telephony.instance;
                  telephony.sendSms(
                    to: "${phone1}",
                    message: '*${password}*10#',
                  );
                });
              }
              //  Get.find<controllerphoneinfo>(tag: 'secend').printHello();
            },
            child: Text(
              'تنظیم ساعت غیرفعالسازی',
              style: textstyle_inqury,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 20,
              widget: Container(),
              text1: ''),
          Container(
            width: Get.width * 0.5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Obx(() {
                  return Checkbox(
                      value: Get.find<controllerphoneinfo>(tag: 'secend')
                          .smartOn
                          .value,
                      onChanged: (value) async {
                        var prefs = await SharedPreferences.getInstance();
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .smartOn
                            .value = value!;

                        prefs.setBool('smartOn', value);
                      });
                }),
                Text(
                  'فعالسازی خودکار',
                  style: textstyle_inqury,
                )
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 20,
              widget: Container(),
              text1: ''),
          Container(
            width: Get.width * 0.5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Obx(() {
                  return Checkbox(
                      value: Get.find<controllerphoneinfo>(tag: 'secend')
                          .smartOff
                          .value,
                      onChanged: (value) async {
                        var prefs = await SharedPreferences.getInstance();
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .smartOff
                            .value = value!;

                        prefs.setBool('smartOff', value);
                      });
                }),
                Text(
                  'غیرفعالسازی خودکار',
                  style: textstyle_inqury,
                )
              ],
            ),
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          Container(
            width: Get.width * 0.9,
            child: Center(
                child: Text(
              '*تــوجـــه*\nبرای هر تغییراتی در خودکارسازی برای ثبت و اعمال شدن بایست یکبار کامل از اپ خارج شده و برنامه بسته شود و دوباره وارد شده',
              style: textstyle_inqury,
            )),
          ),
        ],
      )),
    );
  }
}
