// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';

class SettingInfoSim extends StatefulWidget {
  const SettingInfoSim({Key? key}) : super(key: key);

  @override
  State<SettingInfoSim> createState() => _SettingInfoSimState();
}

class _SettingInfoSimState extends State<SettingInfoSim> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'ویرایش اطلاعات دستگاه',
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 20,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () async {
              TextEditingController controller1 = TextEditingController();
              Get.defaultDialog(
                  backgroundColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![1],
                  title: 'تغییر شماره تلفن',
                  titleStyle: textstyle_inqury,
                  middleTextStyle: textstyle_inqury,
                  //    middleText: 'پیامک فرستاده شود؟',
                  content: Container(
                    width: Get.width * 0.6,
                    child: TextField(
                      controller: controller1,
                    ),
                  ),
                  confirmTextColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                  buttonColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                  textConfirm: 'تغییر',
                  onConfirm: () {
                    Get.find<controllerphoneinfo>(tag: 'secend').phone.value =
                        controller1.text;
                    Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
                    Get.back();
                    Get.snackbar('اطلاعیه',
                        'تغییرات مورد نظر اعمال شد لطفا برنامه را یکبار کامل ببندید دوباره وارد شوید');
                  });
            },
            child: Center(
                child: Text(
              'تغییر شماره تلفن دستگاه',
              style: textstyle_inqury,
            )),
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () async {
              TextEditingController controller2 = TextEditingController();
              Get.defaultDialog(
                  backgroundColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![1],
                  title: 'تغییر  نام',
                  titleStyle: textstyle_inqury,
                  middleTextStyle: textstyle_inqury,
                  //  middleText: 'پیامک فرستاده شود؟',
                  content: Container(
                    width: Get.width * 0.6,
                    child: TextField(
                      controller: controller2,
                    ),
                  ),
                  confirmTextColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                  buttonColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                  textConfirm: 'تغییر',
                  onConfirm: () {
                    Get.find<controllerphoneinfo>(tag: 'secend').Name.value =
                        controller2.text;
                    Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
                    Get.back();
                    Get.snackbar('اطلاعیه',
                        'تغییرات مورد نظر اعمال شد لطفا برنامه را یکبار کامل ببندید دوباره وارد شوید');
                  });
            },
            child: Center(
                child: Text(
              'تغییر نام دستگاه فعلی',
              style: textstyle_inqury,
            )),
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () => Get.defaultDialog(
                backgroundColor: theme[
                    Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![1],
                title: 'هشدار',
                titleStyle: textstyle_inqury,
                middleTextStyle: textstyle_inqury,
                middleText: 'از حذف دستگاه مطمعن هستید؟',
                confirmTextColor: theme[
                    Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                buttonColor: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                textConfirm: 'بله',
                onConfirm: () {
                  Get.find<controllerphoneinfo>(tag: 'secend').Deletephons();
                  Get.back();
                  Get.snackbar('اطلاعیه',
                      'تغییرات مورد نظر اعمال شد لطفا برنامه را یکبار کامل ببندید دوباره وارد شوید');
                }),
            child: Center(
                child: Text(
              'حذف دستگاه فعلی',
              style: textstyle_inqury,
            )),
          ),
        ],
      )),
    );
  }
}
