// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_app/ChangePasswordDevices.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';
import 'EditInfoSim.dart';

class SettingApp extends StatefulWidget {
  const SettingApp({Key? key}) : super(key: key);

  @override
  State<SettingApp> createState() => _SettingAppState();
}

class _SettingAppState extends State<SettingApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'تنظیمات  دستگاه',
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
            InkWell(
              onTap: () => Get.to(SettingInfoSim()),
              child: Box_All(
                  width: Get.width * 0.95,
                  hight: 100,
                  widget: Center(
                      child: Text('ویرایش اطلاعات دستگاه',
                          style: textstyle_inqury)),
                  text1: ''),
            ),
            InkWell(
              onTap: () => Get.to(ChangePasswordDevices()),
              child: Box_All(
                  width: Get.width * 0.95,
                  hight: 100,
                  widget: Center(
                    child: Text(
                      'تغییر رمز دستگاه',
                      style: textstyle_inqury,
                    ),
                  ),
                  text1: ''),
            ),
          ],
        ),
      ),
    );
  }
}
