// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:telephony/telephony.dart';

import '../../Color.dart';

class ChangePasswordDevices extends StatefulWidget {
  const ChangePasswordDevices({Key? key}) : super(key: key);

  @override
  State<ChangePasswordDevices> createState() => _ChangePasswordDevicesState();
}

class _ChangePasswordDevicesState extends State<ChangePasswordDevices> {
  late TextEditingController pass1, pass2, pass3;
  @override
  void initState() {
    pass1 = TextEditingController();
    pass2 = TextEditingController();
    pass3 = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'تغییر رمز دستگاه',
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
          ),
          textfield_1(
            controller: pass1,
          ),
          SizedBox(
            height: 15,
          ),
          textfield_2(
            controller: pass2,
          ),
          SizedBox(
            height: 15,
          ),
          textfield_3(
            controller: pass3,
          ),
          SizedBox(
            height: 15,
          ),
          Obx(() {
            return InkWell(
              onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                          .fifteensecends
                          .value >=
                      15
                  ? () async {
                      Get.defaultDialog(
                          backgroundColor: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![1],
                          title: 'هشدار',
                          titleStyle: textstyle_inqury,
                          middleTextStyle: textstyle_inqury,
                          middleText: 'پیامک فرستاده شود؟',
                          confirmTextColor: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2],
                          buttonColor: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![0],
                          textConfirm: 'بله',
                          onConfirm: () async {
                            if (Get.find<controllerphoneinfo>(tag: 'secend')
                                        .Password
                                        .value ==
                                    pass1.text &&
                                pass2.text == pass3.text) {
                              Get.back();
                              final Telephony telephony = Telephony.instance;
                              telephony.sendSms(
                                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                message:
                                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*40*${pass2.text}#',
                              );
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .Password
                                  .value = pass2.text;
                              Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .updatePhone();
                              var prefs = await SharedPreferences.getInstance();

                              prefs.setString('password_1', pass2.text);
                            } else {
                              Get.snackbar(
                                  'خطا', 'فیلد هارا دوباره و صحیح وارد کنید');
                            }
                          });
                    }
                  : () => Get.snackbar(
                      'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
              child: Container(
                width: Get.width * 0.3,
                height: Get.width * 0.15,
                decoration: BoxDecoration(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    borderRadius: BorderRadius.circular(100)),
                child: FittedBox(
                  child: Center(
                    child: Text(
                      'تایید',
                      style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![1],
                      ),
                    ),
                  ),
                ),
              ),
            );
          })
        ],
      )),
    );
  }
}

class textfield_1 extends StatelessWidget {
  const textfield_1({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.9,
      height: Get.height * 0.1,
      decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
          ),
          borderRadius: BorderRadius.circular(40)),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: TextField(
            controller: controller,
            style: textstyle_inqury,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'رمز فعلی را وارد کنید',
              hintStyle: textstyle_inqury,
            ),
            maxLength: 4,
          ),
        ),
      ),
    );
  }
}

///
///
///

class textfield_2 extends StatelessWidget {
  const textfield_2({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.9,
      height: Get.height * 0.1,
      decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
          ),
          borderRadius: BorderRadius.circular(40)),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: TextField(
            controller: controller,
            style: textstyle_inqury,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'رمز جدید را وارد کنید',
              hintStyle: textstyle_inqury,
            ),
            maxLength: 4,
          ),
        ),
      ),
    );
  }
}

///
///
///
///

class textfield_3 extends StatelessWidget {
  const textfield_3({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.9,
      height: Get.height * 0.1,
      decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
          ),
          borderRadius: BorderRadius.circular(40)),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
          ),
          child: TextField(
            controller: controller,
            style: textstyle_inqury,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'رمز جدید را دوباره وارد کنید',
              hintStyle: textstyle_inqury,
            ),
            maxLength: 4,
          ),
        ),
      ),
    );
  }
}
