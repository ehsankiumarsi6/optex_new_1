import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '/Pages/guarantee/Guarantee.dart';

import '../../Color.dart';

class drawer extends StatelessWidget {
  const drawer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.5,
      height: Get.height,
      decoration: BoxDecoration(
          gradient: LinearGradient(
        //begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],

          //colorred,
        ],
      )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Container(
                // padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                width: Get.width * 0.25,
                height: Get.width * 0.25,
                decoration: BoxDecoration(
                  color: Color(0xFF414249),
                  borderRadius: BorderRadius.circular(80),
                ),
                child: Image.asset('images/logo_red.png'),
                /* child: Center(
                  child: Obx(() {
                    return Text(
                      Get.find<controllerPhone>(tag: 'secend').Name.value == ''
                          ? 'نام دستگاه'
                          : Get.find<controllerPhone>(tag: 'secend').Name.value,
                      style: TextStyle(color: Color(0xFF000000)),
                    );
                  }),
                ), */
              ),
              Transform.translate(
                  offset: Offset(-Get.width * 0.1, -15),
                  child: InkWell(
                    onTap: () async {
                      Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value ==
                              'blue'
                          ? Get.find<controllerphoneinfo>(tag: 'secend')
                              .theme
                              .value = 'red'
                          : Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value ==
                                  'red'
                              ? Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value = 'purple'
                              : Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value = 'blue';

                      ///
                      ///
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value ==
                              'blue'
                          ? prefs.setString('theme', 'blue')
                          : Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value ==
                                  'red'
                              ? prefs.setString('theme', 'red')
                              : prefs.setString('theme', 'purple');
                    },
                    child: Obx(() {
                      return Icon(
                        Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value ==
                                'blue'
                            ? Icons.cloud
                            : Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value ==
                                    'red'
                                ? Icons.dark_mode
                                : Icons.wb_sunny,
                        size: 30,
                        color: Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value ==
                                'blue'
                            ? Colors.blue
                            : Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value ==
                                    'red'
                                ? Colors.grey
                                : Colors.purple,
                      );
                    }),
                  )),
              Center(
                  /* child: Obx(() {
                  return Text(
                    Get.find<controllerPhone>(tag: 'secend').Name.value == ''
                        ? 'نام دستگاه'
                        : Get.find<controllerPhone>(tag: 'secend').Name.value,
                    style: TextStyle(color: Color(0xFF000000)),
                  );
                }), */
                  ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/Add'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.add,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'افزودن دستگاه',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/Contact'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.contact_page,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'مخاطبین',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/SettingApp'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.settings,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'تنظیم دستگاه',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Container(
                width: Get.width * 0.3,
                height: 2,
                color: Color(0xFF049BB6),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/TimeOnOff'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.timelapse_sharp,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'زمان بندی دستگاه',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/Relleh'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.door_back_door_sharp,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'رله',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/SetRemote'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.settings_remote,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'تنظیم ریموت',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Container(
                width: Get.width * 0.3,
                height: 2,
                color: Color(0xFF049BB6),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/SettingDevices'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.settings,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'ابزار پیشرفته',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/SetHalfOn'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.remove_red_eye,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'تنظیم نیمه فعاسازی',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/SettingApp2'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.settings_applications,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'تنظیمات نرم افزار ',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Container(
                width: Get.width * 0.3,
                height: 2,
                color: Color(0xFF049BB6),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.to(Guarantee()),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.change_circle_outlined,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'گارانتی',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/Help'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.help,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'راهنمایی',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: InkWell(
                  onTap: () => Get.toNamed('/zoon'),
                  child: Container(
                      width: Get.width * 0.35,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.home_work_rounded,
                            color: Color(0xFF414249),
                          ),
                          Text(
                            'درباره ما',
                            textDirection: TextDirection.rtl,
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Color(0xFF414249)),
                          ),
                        ],
                      )),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
