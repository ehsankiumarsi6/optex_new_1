// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '/Getxcontroller/controllerphoneinfo.dart';

import '../../Color.dart';
import 'funtion_guarntee.dart';
import 'package:liquid_progress_indicator_v2/liquid_progress_indicator.dart';

class Guarantee extends StatefulWidget {
  const Guarantee({Key? key}) : super(key: key);

  @override
  State<Guarantee> createState() => _GuaranteeState();
}

class _GuaranteeState extends State<Guarantee> {
  late String date, data_guaranti, data_shamsi;
  late TextStyle style1, style2, style3;
  @override
  void initState() {
    final now = DateTime.now();
    date = Get.find<controllerphoneinfo>(tag: 'secend').DateGuarantee.value;
    //'${now.month + 2}/${now.day + 9}/${now.year}';
    date != ''
        ? data_guaranti = calculateDays2(date)
        : data_guaranti = 'شما دستگاهی فعلا ندارید';
    date != ''
        ? data_shamsi = MiladiToShamsi(int.parse(date.split('/')[0]),
            int.parse(date.split('/')[1]), int.parse(date.split('/')[2]))
        : data_shamsi = ' ';
    style1 = TextStyle(
      color:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
      fontSize: 22,
      fontWeight: FontWeight.bold,
    );
    style2 = TextStyle(
        color:
            theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
        fontSize: 16,
        fontWeight: FontWeight.bold);
    style3 = TextStyle(
        color:
            theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
        fontSize: 30,
        fontWeight: FontWeight.bold);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        backgroundColor:
            theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                data_guaranti,
                style: style1,
              ),
              date != ''
                  ? Text(
                      'تاریخ انقضای گارانتی شما',
                      style: style2,
                    )
                  : Text(''),
              Text(
                data_shamsi,
                style: style3,
              ),
              Container(
                width: Get.width * 0.4,
                height: Get.width * 0.15,
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(20)),
                child: LiquidLinearProgressIndicator(
                  borderRadius: 20,
                  center: Text('${(copy_difference / 730 * 100).toInt()}%'),
                  value: copy_difference / 730 * 100,
                  valueColor: AlwaysStoppedAnimation(
                    theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ), // Defaults to the current Theme's accentColor.
                  backgroundColor: theme[
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .theme
                              .value]![
                      1], // Defaults to the current Theme's backgroundColor.
                  borderColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                  borderWidth: 2.0,
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
