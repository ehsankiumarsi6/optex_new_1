// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/testPages/test1.dart';
import 'package:telephony/telephony.dart';

import '../../Color.dart';
import 'function_setting.dart';

class languge extends StatelessWidget {
  const languge({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () async {
                    Get.defaultDialog(
                        backgroundColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![1],
                        title: 'هشدار',
                        titleStyle: textstyle_inqury,
                        middleTextStyle: textstyle_inqury,
                        middleText: 'پیامک فرستاده شود؟',
                        confirmTextColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        buttonColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0],
                        textConfirm: 'بله',
                        onConfirm: () async {
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .Language
                              .value = 'eng';

                          final Telephony telephony = Telephony.instance;
                          telephony.sendSms(
                            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                            message:
                                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*F1#',
                          );
                          Get.back();
                          await Get.find<controllerphoneinfo>(tag: 'secend')
                              .updatePhone();
                          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .startfifteensecends();
                        });
                  }
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Obx(() {
              return Container(
                width: 70,
                height: 50,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Get.find<controllerphoneinfo>(tag: 'secend')
                                .Language
                                .value !=
                            'fri'
                        ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0]
                        : null,
                    borderRadius: BorderRadius.circular(80),
                    border: Border.all(
                      width: 2,
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                    )),
                child: FittedBox(
                    child: Text(
                  'انگلیسی',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2]),
                )),
              );
            }),
          );
        }),
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () async {
                    Get.defaultDialog(
                        backgroundColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![1],
                        title: 'هشدار',
                        titleStyle: textstyle_inqury,
                        middleTextStyle: textstyle_inqury,
                        middleText: 'پیامک فرستاده شود؟',
                        confirmTextColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        buttonColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0],
                        textConfirm: 'بله',
                        onConfirm: () async {
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .Language
                              .value = 'fri';

                          final Telephony telephony = Telephony.instance;
                          telephony.sendSms(
                            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                            message:
                                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*F0#',
                          );
                          Get.back();
                          await Get.find<controllerphoneinfo>(tag: 'secend')
                              .updatePhone();
                          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .startfifteensecends();
                        });
                  }
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Obx(() {
              return Container(
                width: 70,
                height: 50,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(80),
                    color: Get.find<controllerphoneinfo>(tag: 'secend')
                                .Language
                                .value ==
                            'fri'
                        ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0]
                        : null,
                    border: Border.all(
                      width: 2,
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                    )),
                child: FittedBox(
                    child: Text(
                  'فارسی',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2]),
                )),
              );
            }),
          );
        }),
      ],
    );
  }
}

///
///
///

class call_emergency extends StatelessWidget {
  const call_emergency({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () async {
                    Get.defaultDialog(
                        backgroundColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![1],
                        title: 'هشدار',
                        titleStyle: textstyle_inqury,
                        middleTextStyle: textstyle_inqury,
                        middleText: 'پیامک فرستاده شود؟',
                        confirmTextColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        buttonColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0],
                        textConfirm: 'بله',
                        onConfirm: () async {
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .CallOnElectricity
                              .value = 'on';

                          final Telephony telephony = Telephony.instance;
                          telephony.sendSms(
                            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                            message:
                                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*55*1#',
                          );
                          Get.back();
                          await Get.find<controllerphoneinfo>(tag: 'secend')
                              .updatePhone();
                          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .startfifteensecends();
                        });
                  }
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Obx(() {
              return Container(
                width: 70,
                height: 50,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(80),
                    color: Get.find<controllerphoneinfo>(tag: 'secend')
                                .CallOnElectricity
                                .value ==
                            'on'
                        ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0]
                        : null,
                    border: Border.all(
                      width: 2,
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                    )),
                child: FittedBox(
                    child: Text(
                  'فعال',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2]),
                )),
              );
            }),
          );
        }),
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () async {
                    Get.defaultDialog(
                        backgroundColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![1],
                        title: 'هشدار',
                        titleStyle: textstyle_inqury,
                        middleTextStyle: textstyle_inqury,
                        middleText: 'پیامک فرستاده شود؟',
                        confirmTextColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        buttonColor: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0],
                        textConfirm: 'بله',
                        onConfirm: () async {
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .CallOnElectricity
                              .value = 'off';

                          final Telephony telephony = Telephony.instance;
                          telephony.sendSms(
                            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                            message:
                                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*55*0#',
                          );
                          Get.back();
                          await Get.find<controllerphoneinfo>(tag: 'secend')
                              .updatePhone();
                          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                          Get.find<controllerphoneinfo>(tag: 'secend')
                              .startfifteensecends();
                        });
                  }
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Obx(() {
              return Container(
                width: 70,
                height: 50,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Get.find<controllerphoneinfo>(tag: 'secend')
                                .CallOnElectricity
                                .value ==
                            'off'
                        ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0]
                        : null,
                    borderRadius: BorderRadius.circular(80),
                    border: Border.all(
                      width: 2,
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                    )),
                child: FittedBox(
                    child: Text(
                  'غیرفعال',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2]),
                )),
              );
            }),
          );
        }),
      ],
    );
  }
}

///
///
///
///

class time_alarm extends StatelessWidget {
  const time_alarm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Obx(() {
          return Text(
            'مقدار فعلی:${Get.find<controllerphoneinfo>(tag: 'secend').AlarmTime.value} دقیقه',
            style: TextStyle(
              color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
            ),
          );
        }),
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () => TimeAlert()
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Container(
              width: 70,
              height: 50,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(80),
                  border: Border.all(
                    width: 2,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  )),
              child: FittedBox(child: Obx(() {
                return Text(
                  'ویرایش',
                  style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                  ),
                );
              })),
            ),
          );
        }),
      ],
    );
  }
}

///
///
///
class mood_alarm extends StatelessWidget {
  const mood_alarm({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Obx(() {
          List<String> modealarm2 = [
            'ابتدا پیامک سپس تماس با تکرار',
            'ابتدا تماس سپس پیامک با تکرار',
            'ابندا پیامک سپس تماس',
            'ابتدا تماس سپس پیامک',
          ];
          return Text(
            'مقدار فعلی:${modealarm2[int.parse(Get.find<controllerphoneinfo>(tag: 'secend').MoodAlarm.value)]}',
            style: TextStyle(
              color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
            ),
          );
        }),
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () => ModeAlert()
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Container(
              width: 70,
              height: 50,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(80),
                  border: Border.all(
                    width: 2,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  )),
              child: FittedBox(child: Obx(() {
                return Text(
                  'ویرایش',
                  style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                  ),
                );
              })),
            ),
          );
        }),
      ],
    );
  }
}

///
///
///
///
///
class inkiling_holding extends StatelessWidget {
  const inkiling_holding({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Obx(() {
          return Text(
            'مقدار فعلی:${Get.find<controllerphoneinfo>(tag: 'secend').PeriodicInventoryReport.value} پیامک',
            style: TextStyle(
              color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
            ),
          );
        }),
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () => Reportsim()
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Container(
              width: 70,
              height: 50,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(80),
                  border: Border.all(
                    width: 2,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  )),
              child: FittedBox(child: Obx(() {
                return Text(
                  'ویرایش',
                  style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                  ),
                );
              })),
            ),
          );
        }),
      ],
    );
  }
}

///
///
class inkiling_battery extends StatelessWidget {
  const inkiling_battery({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Obx(() {
          return Text(
            'مقدار فعلی:${Get.find<controllerphoneinfo>(tag: 'secend').PeriodicBatteryReport.value} پیامک',
            style: TextStyle(
              color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
            ),
          );
        }),
        Obx(() {
          return InkWell(
            onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                        .fifteensecends
                        .value >=
                    15
                ? () => Reportbatery()
                : () =>
                    Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
            child: Container(
              width: 70,
              height: 50,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(80),
                  border: Border.all(
                    width: 2,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  )),
              child: FittedBox(
                  child: Text(
                'ویرایش',
                style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                ),
              )),
            ),
          );
        }),
      ],
    );
  }
}
