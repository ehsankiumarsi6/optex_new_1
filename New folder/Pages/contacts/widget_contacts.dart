import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';
import 'package:telephony/telephony.dart';

import '../../Color.dart';

class contacts_widget extends StatelessWidget {
  const contacts_widget({Key? key, required this.index}) : super(key: key);
  final int index;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Box_All(
          width: Get.width * 0.9,
          hight: 200,
          widget: Container(
            child: Column(
              children: [
                Container(
                  width: Get.width * 0.8,
                  height: 70,
                  child: Center(child: Obx(() {
                    return TextField(
                      controller: Get.find<controllerphoneinfo>(tag: 'secend')
                          .namecontect[index],
                      style: textstyle_inqury,
                      decoration: InputDecoration(
                        hintText: 'نام مخاطب',
                        hintStyle: textstyle_inqury,
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                          ),
                        ),
                      ),
                    );
                  })),
                ),
                Container(
                  width: Get.width * 0.8,
                  height: 70,
                  child: Center(child: Obx(() {
                    return TextField(
                      controller: Get.find<controllerphoneinfo>(tag: 'secend')
                          .phonecontect[index],
                      style: textstyle_inqury,
                      decoration: InputDecoration(
                        hintText: 'شماره تلفن',
                        hintStyle: textstyle_inqury,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                          ),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                          ),
                        ),
                      ),
                    );
                  })),
                )
              ],
            ),
          ),
          text1: '',
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 2,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                  borderRadius: BorderRadius.circular(70)),
              child: Obx(() {
                return DropdownButton(
                  dropdownColor: theme[
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                  underline: Divider(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![1],
                  ),
                  value: Get.find<controllerphoneinfo>(tag: 'secend')
                              .contact1[index] ==
                          ' '
                      ? null
                      : Get.find<controllerphoneinfo>(tag: 'secend')
                          .dropdownValues[index],
                  onChanged: Get.find<controllerphoneinfo>(tag: 'secend')
                              .fifteensecends
                              .value >=
                          15
                      ? (String? newValue) async {
                          Get.defaultDialog(
                              backgroundColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![1],
                              title: 'هشدار',
                              titleStyle: textstyle_inqury,
                              middleTextStyle: textstyle_inqury,
                              middleText: 'پیامک فرستاده شود؟',
                              confirmTextColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![2],
                              buttonColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![0],
                              textConfirm: 'بله',
                              onConfirm: () {
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .dropdownValues[index] = newValue!;

                                Get.find<controllerphoneinfo>(tag: 'secend')
                                        .contact1[index] =
                                    '${Get.find<controllerphoneinfo>(tag: 'secend').namecontect[index].text}+${Get.find<controllerphoneinfo>(tag: 'secend').phonecontect[index].text}+${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[index]}';

                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .updatePhone();
                                final Telephony telephony = Telephony.instance;
                                telephony.sendSms(
                                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                  message:
                                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*31*${index + 1}*${Get.find<controllerphoneinfo>(tag: 'secend').phonecontect[index].text}${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[index]}#',
                                );
                                Get.back();
                                Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .startfifteensecends();
                              });
                        }
                      : (String? newvalue) => Get.snackbar(
                          'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                  items: <String>['A', 'B', 'C', 'D']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      alignment: AlignmentDirectional.center,
                      child: Text(
                        value,
                        style: TextStyle(
                          fontSize: 20,
                          color: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2],
                          //fontFamily: 'Paeez'
                        ),
                      ),
                    );
                  }).toList(),
                );
              }),
            ),
            SizedBox(
              width: 40,
            ),
            Obx(() {
              return InkWell(
                onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                            .fifteensecends
                            .value >=
                        15
                    ? () async {
                        Get.defaultDialog(
                            backgroundColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![1],
                            title: 'هشدار',
                            titleStyle: textstyle_inqury,
                            middleTextStyle: textstyle_inqury,
                            middleText: 'پیامک فرستاده شود؟',
                            confirmTextColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                            buttonColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![0],
                            textConfirm: 'بله',
                            onConfirm: () {
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .contact1[index] = ' ';
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .namecontect[index]
                                  .text = '';
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .phonecontect[index]
                                  .text = '';
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .dropdownValues[index] = 'A';
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .updatePhone();
                              final Telephony telephony = Telephony.instance;
                              telephony.sendSms(
                                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                message:
                                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*30*${index + 1}*D#',
                              );
                              Get.back();
                              Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .startfifteensecends();
                            });
                      }
                    : () => Get.snackbar(
                        'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                child: Icon(
                  Icons.delete,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                  size: 35,
                ),
              );
            })
          ],
        )
      ],
    );
  }
}
