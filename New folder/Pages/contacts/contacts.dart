import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerOnOff.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/contacts/widget_contacts.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/testPages/test1.dart';
import 'package:telephony/telephony.dart';

import '../../Color.dart';

class Contacts extends StatefulWidget {
  const Contacts({Key? key}) : super(key: key);

  @override
  State<Contacts> createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(3.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'اطلاعات دستکاه',
                      style: textstyle_inqury,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.help,
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                    )
                  ],
                ),
              ),
              Obx(() {
                return Visibility(
                  visible: Get.find<controlleronoff>(tag: 'secend')
                      .Inquirycontect
                      .value,
                  child: CircularProgressIndicator(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                  replacement: Obx(() {
                    return InkWell(
                      onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                                  .fifteensecends
                                  .value >=
                              15
                          ? () async {
                              Get.defaultDialog(
                                  backgroundColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![1],
                                  title: 'هشدار',
                                  titleStyle: textstyle_inqury,
                                  middleTextStyle: textstyle_inqury,
                                  middleText: 'پیامک فرستاده شود؟',
                                  confirmTextColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![2],
                                  buttonColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![0],
                                  textConfirm: 'بله',
                                  onConfirm: () {
                                    final Telephony telephony =
                                        Telephony.instance;
                                    //  String copyMassege = '0,1,1110,0,1,30,1,1,99,0';
                                    telephony.sendSms(
                                      to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                      message:
                                          //'*09307112409B1#',
                                          '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*91#',
                                    );
                                    Get.back();
                                    Get.find<controlleronoff>(tag: 'secend')
                                        .Inquirycontect
                                        .value = true;

                                    Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .startfifteensecends();
                                    telephony.listenIncomingSms(
                                        onNewMessage: (SmsMessage message) {
                                          // Handle message
                                          if (message.address ==
                                                  "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}" &&
                                              message.body!.split('*0').length >
                                                  1) {
                                            Get.find<controlleronoff>(
                                                    tag: 'secend')
                                                .Inquirycontect
                                                .value = false;
                                            Get.snackbar(
                                                'اطلاعیه', 'پیامک دریافت شد');
                                            List<String> copycontexts =
                                                message.body!.split('*');
                                            for (var i = 1;
                                                i < copycontexts.length;
                                                i++) {
                                              String copyphonecontext = '';
                                              for (var j = 0; j < 11; j++) {
                                                copyphonecontext =
                                                    copyphonecontext +
                                                        copycontexts[i][j];
                                              }
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .phonecontect[i - 1]
                                                  .text = copyphonecontext;
                                              Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .dropdownValues[i - 1] =
                                                  copycontexts[i][11];
                                              Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .contact1[i - 1] =
                                                  ' +${copyphonecontext}+${copycontexts[i][11]}';
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .updatePhone();
                                            }
                                          }
                                        },
                                        listenInBackground: false);
                                  });
                            }
                          : () => Get.snackbar(
                              'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                      child: Container(
                        width: Get.width * 0.9,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![0],
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: Obx(() {
                            return Text(
                              'استعلام مخاطب',
                              style: TextStyle(
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![2]),
                            );
                          }),
                        ),
                      ),
                    );
                  }),
                );
              }),
              Column(
                children:
                    List.generate(8, (index) => contacts_widget(index: index)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
