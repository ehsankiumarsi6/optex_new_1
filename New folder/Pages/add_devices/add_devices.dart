//import 'package:audioplayers/audioplayers.dart';
//import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import '/Getxcontroller/controllerOnOff.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/contacts/contacts.dart';
import '/Pages/homepage/HomePage.dart';
import '/Pages/homepage/widget.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import '../../Color.dart';

//import 'package:assets_audio_player/assets_audio_player.dart';
//import 'package:assets_audio_player/assets_audio_player.dart';
//import 'package:just_audio/just_audio.dart';
import 'dart:ui';

class AddDevices extends StatefulWidget {
  const AddDevices({Key? key}) : super(key: key);

  @override
  State<AddDevices> createState() => _AddDevicesState();
}

class _AddDevicesState extends State<AddDevices> {
  late stt.SpeechToText _speech;
  bool _isListening = false;

  String _text = '';
  late final FlutterTts flutterTts1;
  @override
  void initState() {
    _speech = stt.SpeechToText();
    flutterTts1 = FlutterTts();
    audio_seturi_play();

    // TODO: implement initState
    super.initState();
  }

  Future<void> playSoundFile() async {
    await flutterTts1.setLanguage('en-US');
    await flutterTts1.setSpeechRate(0.3);
    await flutterTts1.setVolume(1.0);

    await flutterTts1
        .awaitSpeakCompletion(true); // منتظر تکمیل پخش شدن صوت باشید

    //await flutterTts1.setVoice(voice); // پخش فایل صوتی
  }

  void audio_seturi_play() async {
    if (Get.find<controllerphoneinfo>(tag: 'secend').lenghtmainpage.value ==
        0) {
      final player = AudioPlayer();
      await player.setAsset('music/welcome_optex.mp3');
      final player2 = AudioPlayer();
      await player2.setAsset('music/name_device.mp3');
      print('yes');
      await player.play();

      player2.play();
    }
  }

  void _listen() async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  sst() async {
    if (Get.find<controllerphoneinfo>(tag: 'secend').textnamephone.text == '') {
      final player3 = AudioPlayer();
      await player3.setAsset('music/name_device.mp3');
      player3.play();
      print('yes');
      _listen();
      if (Get.find<controllerphoneinfo>(tag: 'secend').textnamephone.text ==
          '') {
        Get.find<controllerphoneinfo>(tag: 'secend').textnamephone.text ==
            _text;
      }
    } else {
      if (Get.find<controllerphoneinfo>(tag: 'secend').textphone.text == '') {
        final player4 = AudioPlayer();
        await player4.setAsset('music/phone_device.mp3');
        player4.play();
        print('yes');
        _listen();
        if (Get.find<controllerphoneinfo>(tag: 'secend').textphone.text == '') {
          Get.find<controllerphoneinfo>(tag: 'secend').textphone.text == _text;
        }
      } else {
        final player5 = AudioPlayer();
        await player5.setAsset('music/doit_create.mp3');
        player5.play();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      floatingActionButton: FloatingActionButton(
        onPressed: sst,
        child: Icon(Icons.voice_chat),
      ),
      body: SafeArea(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'اطلاعات دستکاه',
                    style: textstyle_inqury,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.help,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 100,
            ),
            Container(
              width: Get.width * 0.7,
              height: Get.height * 0.1,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                      width: 3.5),
                  borderRadius: BorderRadius.circular(40)),
              child: Center(
                child: TextField(
                  style: textstyle_inqury,
                  controller: Get.find<controllerphoneinfo>(tag: 'secend')
                      .textnamephone,
                  decoration: InputDecoration(
                    hintText: 'نام دستگاه',
                    hintStyle: textstyle_inqury,
                    // alignLabelWithHint: true
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: Get.width * 0.7,
              height: Get.height * 0.1,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                      width: 3.5),
                  borderRadius: BorderRadius.circular(40)),
              child: Center(
                child: TextField(
                  style: textstyle_inqury,
                  keyboardType: TextInputType.phone,
                  controller:
                      Get.find<controllerphoneinfo>(tag: 'secend').textphone,
                  decoration: InputDecoration(
                    hintText: '099..شماره تلفن دستگاه',
                    hintStyle: textstyle_inqury,
                    // alignLabelWithHint: true
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: Get.width * 0.65,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () => Get.find<controlleronoff>(tag: 'secend')
                        .opretorsim
                        .value = 'ha',
                    child: Obx(() {
                      return Container(
                        width: Get.width * 0.17,
                        padding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Get.find<controlleronoff>(tag: 'secend')
                                        .opretorsim
                                        .value ==
                                    'ha'
                                ? Colors.lightBlue
                                : null,
                            border: Border.all(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0],
                                width: 1.7),
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: FittedBox(
                              child: Text(
                            'همراه اول',
                            style: textstyle_inqury,
                            textAlign: TextAlign.center,
                          )),
                        ),
                      );
                    }),
                  ),
                  InkWell(
                    onTap: () => Get.find<controlleronoff>(tag: 'secend')
                        .opretorsim
                        .value = 'rl',
                    child: Obx(() {
                      return Container(
                        width: Get.width * 0.17,
                        padding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Get.find<controlleronoff>(tag: 'secend')
                                        .opretorsim
                                        .value ==
                                    'rl'
                                ? Colors.purple[100]
                                : null,
                            border: Border.all(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0],
                                width: 1.7),
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: FittedBox(
                              child: Text(
                            'رایتل',
                            style: textstyle_inqury,
                            textAlign: TextAlign.center,
                          )),
                        ),
                      );
                    }),
                  ),
                  InkWell(
                    onTap: () => Get.find<controlleronoff>(tag: 'secend')
                        .opretorsim
                        .value = 'ir',
                    child: Obx(() {
                      return Container(
                        width: Get.width * 0.17,
                        padding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Get.find<controlleronoff>(tag: 'secend')
                                        .opretorsim
                                        .value ==
                                    'ir'
                                ? Colors.yellow[400]
                                : null,
                            border: Border.all(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0],
                                width: 1.7),
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: FittedBox(
                              child: Text(
                            'ایرانسل',
                            style: textstyle_inqury,
                            textAlign: TextAlign.center,
                          )),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'از ساخت دستگاه مطمعن هستید؟',
                    /*  onCancel: () => Get.,
                                        textCancel: 'خیر', */
                    cancelTextColor: Get.find<controlleronoff>(tag: 'secend')
                            .Themecolor[
                        Get.find<controlleronoff>(tag: 'secend').moodcolor]![2],
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () async {
                      Get.back();
                      Get.find<controllerphoneinfo>(tag: 'secend').adddevice();

                      Get.back();
                      Get.off(homepage());
                      await Future.delayed(Duration(seconds: 2));
                      Get.snackbar('توجه', 'گارانتی شما از امروز فعال شد');
                    });
              },
              child: Container(
                  width: Get.width * 0.3,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                  child: Center(
                      child: Text(
                    'ثبت دستگاه',
                    style: textstyle_inqury,
                  ))),
            )
          ],
        ),
      ),
    );
  }
}
