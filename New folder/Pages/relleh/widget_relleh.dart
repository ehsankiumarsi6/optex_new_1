import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Color.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import 'package:telephony/telephony.dart';

class releh1_on extends StatelessWidget {
  const releh1_on({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return InkWell(
        onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                    .fifteensecends
                    .value >=
                15
            ? () async {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'پیامک فرستاده شود؟',
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () {
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .Relleh1
                          .value = 'on';
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .updatePhone();
                      final Telephony telephony = Telephony.instance;
                      telephony.sendSms(
                        to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                        message:
                            '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*20*1*ON#',
                      );
                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .startfifteensecends();
                    });
              }
            : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
        child: Obx(() {
          return Container(
            width: 100,
            height: 77,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Get.find<controllerphoneinfo>(tag: 'secend')
                            .Relleh1
                            .value ==
                        'on'
                    ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0]
                    : null,
                border: Border.all(
                  width: 1,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0],
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: FittedBox(
                child: Text('فعال',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17)),
              ),
            ),
          );
        }),
      );
    });
  }
}

class releh1_off extends StatelessWidget {
  const releh1_off({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return InkWell(
        onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                    .fifteensecends
                    .value >=
                15
            ? () async {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'پیامک فرستاده شود؟',
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () {
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .Relleh1
                          .value = 'off';

                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .updatePhone();
                      final Telephony telephony = Telephony.instance;
                      telephony.sendSms(
                        to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                        message:
                            '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*20*1*OFF#',
                      );

                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .startfifteensecends();
                    });
              }
            : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
        child: Obx(() {
          return Container(
            width: 100,
            height: 77,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Get.find<controllerphoneinfo>(tag: 'secend')
                            .Relleh1
                            .value ==
                        'off'
                    ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0]
                    : null,
                border: Border.all(
                  width: 1,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0],
                )),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: FittedBox(
                child: Text('غیرفعال',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17)),
              ),
            ),
          );
        }),
      );
    });
  }
}

class releh1_open extends StatelessWidget {
  const releh1_open({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return InkWell(
        onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                    .fifteensecends
                    .value >=
                15
            ? () async {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'پیامک فرستاده شود؟',
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () {
                      final Telephony telephony = Telephony.instance;
                      telephony.sendSms(
                        to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                        message:
                            '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*20*1*T${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[2]}${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[1]}${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[0]}#',
                      );
                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                    });
              }
            : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
        child: Container(
          width: 100,
          height: 77,
          decoration: BoxDecoration(
              /*  color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0], */
              borderRadius: BorderRadius.circular(100),
              border: Border.all(
                width: 2,
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
              )),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: FittedBox(
              child: Text('درب بازکن',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                      fontSize: 17)),
            ),
          ),
        ),
      );
    });
  }
}

///
///
///
class releh2_on extends StatelessWidget {
  const releh2_on({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return InkWell(
        onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                    .fifteensecends
                    .value >=
                15
            ? () async {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'پیامک فرستاده شود؟',
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () {
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .Relleh2
                          .value = 'on';
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .updatePhone();
                      final Telephony telephony = Telephony.instance;
                      telephony.sendSms(
                        to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                        message:
                            '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*20*2*ON#',
                      );
                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .startfifteensecends();
                    });
              }
            : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
        child: Obx(() {
          return Container(
            width: 100,
            height: 77,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Get.find<controllerphoneinfo>(tag: 'secend')
                            .Relleh2
                            .value ==
                        'on'
                    ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0]
                    : null,
                border: Border.all(
                  width: 1,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0],
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: FittedBox(
                child: Text('فعال',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17)),
              ),
            ),
          );
        }),
      );
    });
  }
}

class releh2_off extends StatelessWidget {
  const releh2_off({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return InkWell(
        onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                    .fifteensecends
                    .value >=
                15
            ? () async {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'پیامک فرستاده شود؟',
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () {
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .Relleh2
                          .value = 'off';

                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .updatePhone();
                      final Telephony telephony = Telephony.instance;
                      telephony.sendSms(
                        to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                        message:
                            '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*20*2*OFF#',
                      );

                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .startfifteensecends();
                    });
              }
            : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
        child: Obx(() {
          return Container(
            width: 100,
            height: 77,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Get.find<controllerphoneinfo>(tag: 'secend')
                            .Relleh2
                            .value ==
                        'off'
                    ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0]
                    : null,
                border: Border.all(
                  width: 1,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0],
                )),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: FittedBox(
                child: Text('غیرفعال',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17)),
              ),
            ),
          );
        }),
      );
    });
  }
}

class releh2_open extends StatelessWidget {
  const releh2_open({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return InkWell(
        onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                    .fifteensecends
                    .value >=
                15
            ? () async {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'پیامک فرستاده شود؟',
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () {
                      final Telephony telephony = Telephony.instance;
                      telephony.sendSms(
                        to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                        message:
                            '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*20*2*T${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[2]}${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[1]}${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[0]}#',
                      );
                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                    });
              }
            : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
        child: Container(
          width: 100,
          height: 77,
          decoration: BoxDecoration(
              /*  color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0], */
              borderRadius: BorderRadius.circular(100),
              border: Border.all(
                width: 2,
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
              )),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: FittedBox(
              child: Text('درب بازکن',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                      fontSize: 17)),
            ),
          ),
        ),
      );
    });
  }
}

///
///
///
///
///

void timerelleh1() {
  Get.defaultDialog(
    backgroundColor:
        theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
    title: 'زمان درب بازکن رله 1',
    titleStyle: TextStyle(
      color:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
    ),
    content: Container(
      width: Get.width * 0.7,
      child: Column(
        children: [
          Text(
            'زمان هارا بصورت دو رقمی وارد کنید مانند:02,01,..',
            style: TextStyle(
              color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            ),
          ),
          TextField(
            autofocus: true,
            controller:
                Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[0],
            keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![2],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText:
                    'ساعت${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[2]} ',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                    fontSize: 16)),
          ),
          TextField(
            autofocus: true,
            controller:
                Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[1],
            keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText:
                    'دقیقه${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[1]} ',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    fontSize: 16)),
          ),
          TextField(
            controller:
                Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[2],
            keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText:
                    'ثانیه${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[0]} ',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    fontSize: 16)),
          ),
          InkWell(
            onTap: () {
              if (Get.find<controllerphoneinfo>(tag: 'secend')
                          .Rellehtime[2]
                          .text
                          .length ==
                      2 &&
                  Get.find<controllerphoneinfo>(tag: 'secend')
                          .Rellehtime[1]
                          .text
                          .length ==
                      2 &&
                  Get.find<controllerphoneinfo>(tag: 'secend')
                          .Rellehtime[0]
                          .text
                          .length ==
                      2) {
                Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1] =
                    '${Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[2].text},${Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[1].text},${Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[0].text}';
                Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
                Get.snackbar('تایید', 'تغییرات اعمال شد');
              } else {
                Get.snackbar('خطا', 'دو رقمی لطفا وارد کنید');
              }
            },
            child: Container(
              decoration: BoxDecoration(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              child: Text(
                'تایید',
                style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

///
///
///

void timerelleh2() {
  Get.defaultDialog(
    backgroundColor:
        theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
    title: 'زمان درب بازکن رله 2',
    titleStyle: TextStyle(
      color:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
    ),
    content: Container(
      width: Get.width * 0.7,
      child: Column(
        children: [
          Text(
            'زمان هارا بصورت دو رقمی وارد کنید مانند:02,01,..',
            style: TextStyle(
              color: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            ),
          ),
          TextField(
            autofocus: true,
            controller:
                Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[0],
            keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![2],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText:
                    'ساعت${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[2]} ',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                    fontSize: 16)),
          ),
          TextField(
            autofocus: true,
            controller:
                Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[1],
            keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText:
                    'دقیقه${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[1]} ',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    fontSize: 16)),
          ),
          TextField(
            controller:
                Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[2],
            keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText:
                    'ثانیه${Get.find<controllerphoneinfo>(tag: 'secend').Relehs[1].split(',')[0]} ',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    fontSize: 16)),
          ),
          InkWell(
            onTap: () {
              if (Get.find<controllerphoneinfo>(tag: 'secend')
                          .Rellehtime[2]
                          .text
                          .length ==
                      2 &&
                  Get.find<controllerphoneinfo>(tag: 'secend')
                          .Rellehtime[1]
                          .text
                          .length ==
                      2 &&
                  Get.find<controllerphoneinfo>(tag: 'secend')
                          .Rellehtime[0]
                          .text
                          .length ==
                      2) {
                Get.find<controllerphoneinfo>(tag: 'secend').Relehs[3] =
                    '${Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[5].text},${Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[4].text},${Get.find<controllerphoneinfo>(tag: 'secend').Rellehtime[3].text}';
                Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
                Get.snackbar('تایید', 'تغییرات اعمال شد');
              } else {
                Get.snackbar('خطا', 'دو رقمی لطفا وارد کنید');
              }
            },
            child: Container(
              decoration: BoxDecoration(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              child: Text(
                'تایید',
                style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

///
///
///
///

void namerelleh() {
  TextEditingController namerelleh1 = TextEditingController(),
      namerelleh2 = TextEditingController();
  Get.defaultDialog(
    backgroundColor:
        theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
    title: 'ویرایش نام رله',
    titleStyle: TextStyle(
      color:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
    ),
    content: Container(
      width: Get.width * 0.7,
      child: Column(
        children: [
          TextField(
            autofocus: true,
            controller: namerelleh1,
            // keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText: 'نام رله 1',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    fontSize: 16)),
          ),
          InkWell(
            onTap: () async {
              Get.find<controllerphoneinfo>(tag: 'secend').Relehs[0] =
                  namerelleh1.text;
              Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
            },
            child: Container(
              decoration: BoxDecoration(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              child: Text(
                'تایید',
                style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                ),
              ),
            ),
          ),
          TextField(
            controller: namerelleh2,
            //   keyboardType: TextInputType.phone,
            cursorColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            style: TextStyle(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                fontSize: 20),
            decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                ),
                hintText: 'نام رله 2',
                hintStyle: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    fontSize: 16)),
          ),
          InkWell(
            onTap: () async {
              Get.find<controllerphoneinfo>(tag: 'secend').Relehs[2] =
                  namerelleh2.text;
              Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
            },
            child: Container(
              decoration: BoxDecoration(
                color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                borderRadius: BorderRadius.circular(20),
              ),
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              child: Text(
                'تایید',
                style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

///
///
///
///

void settingrelleh() {
  List<String> typeReleh = [
    'دینگ دانگ',
    'تحریک زون معمولی',
    'زون جاسوسی',
    '24 ساعت',
    'قطع برق',
    'حالت سایلنت',
  ];
  Get.defaultDialog(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      title: 'نوع تحریک رله 1',
      titleStyle: TextStyle(
          color: theme[
              Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2]),
      content: Container(
        width: Get.width * 0.7,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                'یکی از گزینه های زیر را برای تعیین نوع تحریک رله انتخاب کنید',
                style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0]),
              ),
              Text(
                'اگر با این بخش آشنایی ندارید به هیچ عنوان تغییری اعمال نکنید!',
                style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    fontWeight: FontWeight.bold),
              ),
              Column(
                children: List.generate(typeReleh.length, (index) {
                  return InkWell(
                    onTap: () {
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .TypeRelleh
                          .value = index;
                      final Telephony telephony = Telephony.instance;
                      telephony.sendSms(
                        to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                        message:
                            '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*29*1*${index + 1}#',
                      );
                      Get.back();
                    },
                    child: Container(
                      padding: EdgeInsets.all(2.0),
                      margin: EdgeInsets.all(2.0),
                      width: Get.width * 0.6,
                      decoration: BoxDecoration(
                          color: Get.find<controllerphoneinfo>(tag: 'secend')
                                      .TypeRelleh
                                      .value ==
                                  index
                              ? theme[Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![0]
                              : theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![1],
                          border: Border.all(
                              color: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![0]),
                          borderRadius: BorderRadius.circular(20)),
                      child: Center(
                        child: Text(
                          typeReleh[index],
                          style: TextStyle(
                              color: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![2],
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  );
                }),
              )
            ],
          ),
        ),
      ));
}
