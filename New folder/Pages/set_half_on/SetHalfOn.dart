// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';
import 'package:telephony/telephony.dart';

import '../../Color.dart';

class SetHalfOn extends StatefulWidget {
  const SetHalfOn({Key? key}) : super(key: key);

  @override
  State<SetHalfOn> createState() => _SetHalfOnState();
}

class _SetHalfOnState extends State<SetHalfOn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'تنظیم نیمه فعالسازی',
                      style: textstyle_inqury,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.help,
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                    )
                  ],
                ),
              ),
              Obx(() {
                return InkWell(
                  onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                              .fifteensecends
                              .value >=
                          15
                      ? () async {
                          Get.defaultDialog(
                              backgroundColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![1],
                              title: 'هشدار',
                              titleStyle: textstyle_inqury,
                              middleTextStyle: textstyle_inqury,
                              middleText: 'پیامک فرستاده شود؟',
                              confirmTextColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![2],
                              buttonColor: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![0],
                              textConfirm: 'بله',
                              onConfirm: () async {
                                String copy = '';
                                for (var i = 0; i < 9; i++) {
                                  copy = copy +
                                      (Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .eyezoon[i] ==
                                              true
                                          ? '1'
                                          : '0');
                                }
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .eyezoonindex
                                    .value = copy;
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .updatePhone();
                                final Telephony telephony = Telephony.instance;
                                telephony.sendSms(
                                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                  message:
                                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*14*$copy#',
                                );
                                Get.back();
                                await Get.find<controllerphoneinfo>(
                                        tag: 'secend')
                                    .updatePhone();
                                Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .startfifteensecends();
                              });
                        }
                      : () => Get.snackbar(
                          'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    decoration: BoxDecoration(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0],
                        borderRadius: BorderRadius.circular(50)),
                    child: Obx(() {
                      return Text(
                        'اعمال کردن تغییرات',
                        style: TextStyle(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2]),
                      );
                    }),
                  ),
                );
              }),
              SizedBox(
                height: 20,
              ),
              Column(
                // crossAxisAlignment: WrapCrossAlignment.center,
                //  spacing: 10,
                children: List.generate(
                    9,
                    (index) => Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                width: Get.width * 0.7,
                                height: 30,
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      //begin: Alignment.topLeft,
                                      end: FractionalOffset.bottomRight,
                                      //  stops: [_animation1.value, 0.5],
                                      colors: [
                                        /* Colors.red,
                      Colors.blue */
                                        // Theme(data: data, child: child)
                                        theme[Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .theme
                                            .value]![1],
                                        theme[Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .theme
                                            .value]![0],
                                      ],
                                    ),
                                    borderRadius: BorderRadius.circular(100)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      '${index + 1}',
                                      style: textstyle_inqury,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(right: 10),
                                      child: Obx(() {
                                        return Text(
                                          Get.find<controllerphoneinfo>(
                                                  tag: 'secend')
                                              .nameZoon[index]
                                              .value,
                                          style: TextStyle(
                                              color: theme[
                                                  Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .theme
                                                      .value]![2]),
                                        );
                                      }),
                                    ),
                                  ],
                                )),
                            Obx(() {
                              return Checkbox(
                                value:
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .eyezoon[index],
                                onChanged: (value) {
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .eyezoon[index] = value!;
                                },
                              );
                            }),
                            InkWell(
                              onTap: () {
                                TextEditingController controllerchangename =
                                    TextEditingController();
                                Get.defaultDialog(
                                    title: 'تغییر نام',
                                    backgroundColor: theme[
                                        Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .theme
                                            .value]![1],
                                    content: Container(
                                      width: Get.width * 0.7,
                                      child: Column(
                                        children: [
                                          TextField(
                                            controller: controllerchangename,
                                            style: TextStyle(
                                                color: theme[Get.find<
                                                            controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .theme
                                                    .value]![2]),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 5),
                                            decoration: BoxDecoration(
                                                color: theme[Get.find<
                                                            controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .theme
                                                    .value]![0],
                                                borderRadius:
                                                    BorderRadius.circular(50)),
                                            child: InkWell(
                                              onTap: () {
                                                print(
                                                    'change name${controllerchangename.text}:${index}');
                                                Get.find<controllerphoneinfo>(
                                                            tag: 'secend')
                                                        .nameZoon[index]
                                                        .value =
                                                    controllerchangename.text;
                                                Get.find<controllerphoneinfo>(
                                                        tag: 'secend')
                                                    .updatePhone();
                                                Get.back();
                                              },
                                              child: Obx(() {
                                                return Text(
                                                  'تغییر',
                                                  style: TextStyle(
                                                      color: theme[Get.find<
                                                                  controllerphoneinfo>(
                                                              tag: 'secend')
                                                          .theme
                                                          .value]![2]),
                                                );
                                              }),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    titleStyle: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2]));
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5),
                                decoration: BoxDecoration(
                                    color: theme[Get.find<controllerphoneinfo>(
                                            tag: 'secend')
                                        .theme
                                        .value]![0],
                                    borderRadius: BorderRadius.circular(50)),
                                child: Obx(() {
                                  return Text(
                                    'تغییر نام',
                                    style: TextStyle(
                                        color: theme[
                                            Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .theme
                                                .value]![2]),
                                  );
                                }),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            )
                          ],
                        )),
              ),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }
}
