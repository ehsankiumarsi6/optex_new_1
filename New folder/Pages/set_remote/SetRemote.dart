// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';
import 'package:telephony/telephony.dart';

import '../../Color.dart';

class SetRemote extends StatefulWidget {
  const SetRemote({Key? key}) : super(key: key);

  @override
  State<SetRemote> createState() => _SetRemoteState();
}

class _SetRemoteState extends State<SetRemote> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'تنظیم ریموت',
                    style: textstyle_inqury,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.help,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Box_All_1(
                width: Get.width * 0.9,
                hight: 25,
                widget: Container(),
                text1: ''),
            Obx(() {
              return InkWell(
                onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                            .fifteensecends
                            .value >=
                        15
                    ? () async {
                        Get.defaultDialog(
                            backgroundColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![1],
                            title: 'هشدار',
                            titleStyle: textstyle_inqury,
                            middleTextStyle: textstyle_inqury,
                            middleText: 'پیامک فرستاده شود؟',
                            confirmTextColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                            buttonColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![0],
                            textConfirm: 'بله',
                            onConfirm: () {
                              final Telephony telephony = Telephony.instance;
                              telephony.sendSms(
                                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                message:
                                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*59*1#',
                              );                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .startfifteensecends();
                            });

                      }
                    : () => Get.snackbar(
                        'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                child: Center(
                    child: Text('فعال کردن ریموت دستگاه',
                        style: TextStyle(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                            fontSize: 17))),
              );
            }),
            SizedBox(
              height: 20,
            ),
            Box_All_1(
                width: Get.width * 0.9,
                hight: 25,
                widget: Container(),
                text1: ''),
            Obx(() {
              return InkWell(
                onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                            .fifteensecends
                            .value >=
                        15
                    ? () async {
                        Get.defaultDialog(
                            backgroundColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![1],
                            title: 'هشدار',
                            titleStyle: textstyle_inqury,
                            middleTextStyle: textstyle_inqury,
                            middleText: 'پیامک فرستاده شود؟',
                            confirmTextColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                            buttonColor: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![0],
                            textConfirm: 'بله',
                            onConfirm: () {
                              final Telephony telephony = Telephony.instance;
                              telephony.sendSms(
                                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                message:
                                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*59*0#',
                              );                      Get.back();
                      Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                      Get.find<controllerphoneinfo>(tag: 'secend')
                          .startfifteensecends();
                            });

                      }
                    : () => Get.snackbar(
                        'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                child: Center(
                    child: Text('غیر فعال کردن ریموت دستگاه',
                        style: TextStyle(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                            fontSize: 17))),
              );
            }),
            SizedBox(
              height: 20,
            ),
            /*   Box_All_1(
                width: Get.width * 0.9,
                hight: 25,
                widget: Container(),
                text1: ''),
            Center(
                child: Text('تنظیم ساعت فعالسازی ریموت',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17))),
            SizedBox(
              height: 20,
            ),
            Box_All_1(
                width: Get.width * 0.9,
                hight: 25,
                widget: Container(),
                text1: ''),
            Center(
                child: Text('تنظیم ساعت غیر فعالسازی ریموت',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17))),
            SizedBox(
              height: 20,
            ),
            Box_All_1(
                width: Get.width * 0.9,
                hight: 20,
                widget: Container(),
                text1: ''),
            Container(
              width: Get.width * 0.5,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Checkbox(value: false, onChanged: (value) {}),
                  Text('فعالسازی خودکار ریموت',
                      style: TextStyle(
                          color: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2],
                          fontSize: 17))
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Box_All_1(
                width: Get.width * 0.9,
                hight: 20,
                widget: Container(),
                text1: ''),
            Container(
              width: Get.width * 0.5,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Checkbox(value: false, onChanged: (value) {}),
                  Text('غیرفعالسازی  خودکار ریموت',
                      style: TextStyle(
                          color: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2],
                          fontSize: 17))
                ],
              ),
            ), */
          ],
        ),
      ),
    );
  }
}
