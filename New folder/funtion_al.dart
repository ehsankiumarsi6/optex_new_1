import 'package:get/get.dart';
import 'package:new_test_3/Color.dart';
import 'package:new_test_3/Getxcontroller/controllerOnOff.dart';
import 'package:new_test_3/Getxcontroller/controllerphoneinfo.dart';
import 'package:new_test_3/Pages/homepage/widget.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:telephony/telephony.dart';

OnPhone() {
  Get.find<controllerphoneinfo>(tag: 'secend').fifteensecends.value >= 15
      ? () async {
          print('OnPhone');
          //   Get.back();
          Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value = 'on';
          final Telephony telephony = Telephony.instance;
          telephony.sendSms(
            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
            message:
                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*11#',
          );
          await Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
          Get.find<controllerphoneinfo>(tag: 'secend').startfifteensecends();
          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
        }
      : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید');
}

OffPhone() {
  Get.find<controllerphoneinfo>(tag: 'secend').fifteensecends.value >= 15
      ? () async {
          //  Get.back();
          Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value = 'off';
          final Telephony telephony = Telephony.instance;
          telephony.sendSms(
            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
            message:
                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*10#',
          );
          await Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
          Get.find<controllerphoneinfo>(tag: 'secend').startfifteensecends();
          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
        }
      : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید');
}

SilentPhone() {
  Get.find<controllerphoneinfo>(tag: 'secend').fifteensecends.value >= 15
      ? () async {
          // Get.back();
          Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value =
              'silent';
          final Telephony telephony = Telephony.instance;
          telephony.sendSms(
            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
            message:
                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*12#',
          );
          await Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
          Get.find<controllerphoneinfo>(tag: 'secend').startfifteensecends();
          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
        }
      : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید');
}

HalfOnPhone() {
  Get.find<controllerphoneinfo>(tag: 'secend').fifteensecends.value >= 15
      ? () async {
          //  Get.back();
          Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value =
              'halfon';
          final Telephony telephony = Telephony.instance;
          telephony.sendSms(
            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
            message:
                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*13#',
          );
          await Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
          Get.find<controllerphoneinfo>(tag: 'secend').startfifteensecends();
          Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
        }
      : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید');
}

Shonud() {
  Get.find<controllerphoneinfo>(tag: 'secend').fifteensecends.value >= 15
      ? () async {
          final Telephony telephony = Telephony.instance;
          telephony.sendSms(
            to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
            message:
                '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*62#',
          );
          // Get.back();
          Get.snackbar('اطلاعیه', 'پیامک ارسال شد');
          Get.find<controllerphoneinfo>(tag: 'secend').startfifteensecends();
        }
      : () => Get.snackbar('خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید');
}

Map<String, Function> ee = {
  'فعال': () {
    print('ehsannnnnnn');
  },
  'غیر غعال': OffPhone(),
  'نیمه فعال': HalfOnPhone(),
  'سایلنت': SilentPhone(),
};
List<String> true_green = [
  'فعال',
  'غیر فعال',
  'نیمه فعال',
  'سایلنت',
  'غیرفعال'
];
late stt.SpeechToText _speech;
listen_homepage() async {
  late stt.SpeechToText _speech = stt.SpeechToText();

  bool available = await _speech.initialize(
    onStatus: (val) => print('onStatus: $val'),
    onError: (val) => print('onError: $val'),
  );
  if (available) {
    //۱۲۳۴۵۶۷۸۹۱۰
    //  _speech.systemLocale() ;
    //  setState(() => _isListening = true);
    _speech.listen(
      localeId: 'fa_IR',
      onResult: (val) async {
        Get.find<controlleronoff>(tag: 'secend').green_listen.value = false;
        Get.find<controlleronoff>(tag: 'secend').text_listen.value =
            val.recognizedWords;
        if (val.hasConfidenceRating && val.confidence > 0) {
          Get.find<controlleronoff>(tag: 'secend').text_listen.value =
              val.recognizedWords;
          final Telephony telephony = Telephony.instance;

          for (var element in true_green) {
            if (element ==
                Get.find<controlleronoff>(tag: 'secend').text_listen.value) {
              Get.find<controlleronoff>(tag: 'secend').green_listen.value =
                  true;

              if (element == 'فعال') {
                Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value =
                    'on';
                final Telephony telephony = Telephony.instance;
                telephony.sendSms(
                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                  message:
                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*11#',
                );
                await Get.find<controllerphoneinfo>(tag: 'secend')
                    .updatePhone();
                Get.find<controllerphoneinfo>(tag: 'secend')
                    .startfifteensecends();
                Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
              }
              if (element == 'غیر فعال') {
                Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value =
                    'off';
                final Telephony telephony = Telephony.instance;
                telephony.sendSms(
                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                  message:
                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*10#',
                );
                await Get.find<controllerphoneinfo>(tag: 'secend')
                    .updatePhone();
                Get.find<controllerphoneinfo>(tag: 'secend')
                    .startfifteensecends();
                Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
              }
              if (element == 'غیرفعال') {
                Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value =
                    'off';
                final Telephony telephony = Telephony.instance;
                telephony.sendSms(
                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                  message:
                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*10#',
                );
                await Get.find<controllerphoneinfo>(tag: 'secend')
                    .updatePhone();
                Get.find<controllerphoneinfo>(tag: 'secend')
                    .startfifteensecends();
                Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
              }
              if (element == 'سایلنت') {
                Get.find<controllerphoneinfo>(tag: 'secend').OnPhones.value =
                    'silent';
                final Telephony telephony = Telephony.instance;
                telephony.sendSms(
                  to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                  message:
                      '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*12#',
                );
                await Get.find<controllerphoneinfo>(tag: 'secend')
                    .updatePhone();
                Get.find<controllerphoneinfo>(tag: 'secend')
                    .startfifteensecends();
                Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
              }
              if (element == 'نیمه فعال') {
                HalfOnPhone();
              }
            }
          }
          // print(_text);
        }
      },
    );

    // تنظیم زبان به فارسی
  }
}
