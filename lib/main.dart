import 'dart:ui';

//import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:new_test_3/Pages/testPages/test2.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:telephony/telephony.dart';
import '/Getxcontroller/mybinding.dart';
//import '/New%20folder/test3.dart';
import '/Pages/add_devices/add_devices.dart';
import '/Pages/contacts/contacts.dart';
import '/Pages/help/help.dart';
import '/Pages/relleh/relleh.dart';
import '/Pages/set_half_on/SetHalfOn.dart';
import '/Pages/set_remote/SetRemote.dart';
import '/Pages/setting_app2/SettingApp2.dart';
import '/Pages/setting_devices/Setting_devices.dart';
import '/Pages/splashscreen/SplashScreen.dart';
import '/Pages/testPages/test1.dart';
import '/Pages/time_onoff/TimeOnOff.dart';
import 'package:android_alarm_manager_plus/android_alarm_manager_plus.dart';
import 'Pages/homepage/HomePage.dart';
import 'Pages/setting_app/SettingApp.dart';

//ehsan
/* 
import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity() {
}
 */
ATU(String Hour, String Minute, String Code, String Atu) async {
  DateTime now = DateTime.now();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool smartOn = prefs.getBool(Atu) ?? false;
  int minute = prefs.getInt(Minute) ?? 20;
  int hour = prefs.getInt(Hour) ?? 4;
  String phone1 = prefs.getString('phone1') ?? '0992';
  String password = prefs.getString('password_1') ?? '0000';
  print('TimeIs${now.hour}:${now.minute}:${now.second}:${now.millisecond}');
  if (smartOn) {
    if (now.hour == hour) {
      if (now.minute == minute ||
          now.minute == (minute + 1) ||
          now.minute == (minute - 1)) {
        print(Code);
        final Telephony telephony = Telephony.instance;
        telephony.sendSms(
          to: "${phone1}",
          message: '*${password}*$Code#',
        );
      } else {
        print('NoMinute${Code}MinuteIs$minute');
      }
    } else {
      print('NoHour${Code}HourIs$hour');
    }
  } else {
    print('NoBool${Code}');
  }
}

_printHello() async {
  await ATU('hour1', 'minute1', '11', 'smartOn');
  await ATU('hour_1', 'minute_1', '10', 'smartOff');
  // await Future.delayed(Duration(seconds: 50), printHello());~
}

main() async {
  // Be sure to add this line if initialize() call happens before runApp()
  WidgetsFlutterBinding.ensureInitialized();

  await AndroidAlarmManager.initialize();

  runApp(MyApp());
  final int helloAlarmID = 0;
  await AndroidAlarmManager.periodic(
      const Duration(seconds: 10), helloAlarmID, _printHello,
      exact: true,
      wakeup: true,
      rescheduleOnReboot: true,
      allowWhileIdle: true);
}

class AppScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}

class MyScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      scrollBehavior: MyScrollBehavior(),
      initialBinding: mybinding(),
      initialRoute: '/SplashScreen',
      //'/SplashScreen',
      //rrr
      getPages: [
        GetPage(name: '/SplashScreen', page: () => Splashscreen()),
        GetPage(name: '/HomePage', page: () => homepage()),
        GetPage(name: '/Add', page: () => AddDevices()),
        GetPage(name: '/Contact', page: () => Contacts()),
        GetPage(name: '/Relleh', page: () => Relleh()),
        GetPage(name: '/SettingDevices', page: () => Setting_devices()),
        GetPage(name: '/SettingApp', page: () => SettingApp()),
        GetPage(name: '/TimeOnOff', page: () => TimeOnOff()),
        GetPage(name: '/SetRemote', page: () => SetRemote()),
        GetPage(name: '/SettingApp2', page: () => SettingApp2()),
        GetPage(name: '/SetHalfOn', page: () => SetHalfOn()),
        GetPage(name: '/Help', page: () => Help()),
        GetPage(name: '/test1', page: () => test1()),
        GetPage(name: '/test2', page: () => test2())
        //   GetPage(name: '/test3', page: () => MyAnimatedGradient())
      ],
    );
  }
}
