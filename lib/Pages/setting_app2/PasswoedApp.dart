// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerOnOff.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_app/ChangePasswordDevices.dart';
//import '/Pages/setting_devices/widget_SettingDevices.dart';
//import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Color.dart';

class PasswordApp extends StatefulWidget {
  const PasswordApp({Key? key}) : super(key: key);

  @override
  State<PasswordApp> createState() => _PasswordAppState();
}

class _PasswordAppState extends State<PasswordApp> {
  late TextEditingController pass1, pass2, pass3;
  @override
  void initState() {
    pass1 = TextEditingController();
    pass2 = TextEditingController();
    pass3 = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'تغییر رمز ورود نرم افزار',
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
          ),
          /* textfield_1(
            controller: pass1,
          ), */
          SizedBox(
            height: 15,
          ),
          textfield_2(
            controller: pass2,
          ),
          SizedBox(
            height: 15,
          ),
          textfield_3(
            controller: pass3,
          ),
          SizedBox(
            height: 15,
          ),
          Obx(() {
            return InkWell(
              onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                          .fifteensecends
                          .value >=
                      15
                  ? () async {
                      Get.defaultDialog(
                          backgroundColor: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![1],
                          title: 'هشدار',
                          titleStyle: textstyle_inqury,
                          middleTextStyle: textstyle_inqury,
                          middleText: 'رمز ورود نرم افزار تغییر کند؟',
                          confirmTextColor: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![2],
                          buttonColor: theme[
                              Get.find<controllerphoneinfo>(tag: 'secend')
                                  .theme
                                  .value]![0],
                          textConfirm: 'بله',
                          onConfirm: () async {
                            if (pass2.text == pass3.text) {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();

                              prefs.setString('password', pass2.text);
                              Get.back();
                              Get.snackbar('هشدار', 'رمز تغییر کرد');
                            } else {
                              Get.snackbar('خطا', 'دو رمز باهم همخوانی ندارند');
                            }
                          });
                    }
                  : () => Get.snackbar(
                      'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
              child: Container(
                width: Get.width * 0.3,
                height: Get.width * 0.15,
                decoration: BoxDecoration(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                    borderRadius: BorderRadius.circular(100)),
                child: FittedBox(
                  child: Center(
                    child: Text(
                      'تایید',
                      style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![1],
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 90,
              widget: Center(
                child: Obx(() {
                  return DropdownButton<String>(
                    dropdownColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    onChanged: (value) async {
                      Get.find<controlleronoff>(tag: 'secend')
                          .valuepassword
                          .value = value!;
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();

                      prefs.setString('valuepassword', value);
                    },
                    value: Get.find<controlleronoff>(tag: 'secend')
                        .valuepassword
                        .value,
                    items: <String>[
                      'اثر انگشت',
                      'رمز عبور',
                      'بدون رمز',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        alignment: AlignmentDirectional.center,
                        child: Obx(() {
                          return Text(
                            value,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              color: theme[
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                      .theme
                                      .value]![2],
                              //fontFamily: 'Paeez'
                            ),
                          );
                        }),
                      );
                    }).toList(),
                  );
                }),
              ),
              text1: ''),
        ],
      )),
    );
  }
}
