// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_app2/PasswoedApp.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';
import 'PersonalityApp.dart';

class SettingApp2 extends StatefulWidget {
  const SettingApp2({Key? key}) : super(key: key);

  @override
  State<SettingApp2> createState() => _SettingApp2State();
}

class _SettingApp2State extends State<SettingApp2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'تنظیمات دستگاه',
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () => Get.to(PasswordApp()),
            child: Center(
                child: Text('تنظیمات رمز عبور',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17))),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () => Get.to(PersonalityApp()),
            child: Center(
                child: Text('شخصی سازی صفحه اصلی',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17))),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () => Get.defaultDialog(
                backgroundColor: theme[
                    Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![1],
                title: 'هشدار',
                titleStyle: textstyle_inqury,
                middleTextStyle: textstyle_inqury,
                middleText: 'از ریست دستگاه مطمعن هستید؟',
                confirmTextColor: theme[
                    Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                buttonColor: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                    .theme
                    .value]![0],
                textConfirm: 'بله',
                onConfirm: () {
                  Get.find<controllerphoneinfo>(tag: 'secend').resetphone();
                  Get.snackbar('توجه',
                      'برای اعمال شدن یکبار دستگاه را ببندید دوباره واردشوید');
                }),
            child: Center(child: Obx(() {
              return Text(
                  'ریست تنظیمات دستگاه${Get.find<controllerphoneinfo>(tag: 'secend').Name.value}',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                      fontSize: 17));
            })),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () => delete_device(),
            child: Center(child: Obx(() {
              return Text(
                  'حذف دستگاه ${Get.find<controllerphoneinfo>(tag: 'secend').Name.value}',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                      fontSize: 17));
            })),
          ),
          SizedBox(
            height: 20,
          ),
          Box_All_1(
              width: Get.width * 0.9,
              hight: 25,
              widget: Container(),
              text1: ''),
          InkWell(
            onTap: () => set_capsol(),
            child: Center(child: Obx(() {
              return Text(
                  'تنظیم بازه کپسول شارژ دستگاه:${Get.find<controllerphoneinfo>(tag: 'secend').Name.value}',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                      fontSize: 17));
            })),
          ),
          SizedBox(
            height: 20,
          )
        ],
      )),
    );
  }
}

TextEditingController maxcapsole = TextEditingController();
TextEditingController mincapsole = TextEditingController();
set_capsol() => Get.defaultDialog(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      title: 'تنظیم بازه کپسول شارژ',
      titleStyle: TextStyle(
          color: theme[
              Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2]),
      content: Container(
        width: Get.width * 0.7,
        child: Column(
          children: [
            Text(
              'کپسول در حداکثر شارژ پر نمایش داده میشود.همچنین در حداقل شارژ کاملا خالی نمایش داده می شود',
              style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0]),
            ),
            TextField(
              autofocus: true,
              controller: maxcapsole,
              keyboardType: TextInputType.phone,
              cursorColor: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
              style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                  fontSize: 20),
              decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2]),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0]),
                  ),
                  hintText:
                      'تومان${Get.find<controllerphoneinfo>(tag: 'secend').Chargemax.value}حداکثر شارژ',
                  hintStyle: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                      fontSize: 16)),
            ),
            TextField(
              controller: mincapsole,
              keyboardType: TextInputType.phone,
              cursorColor: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
              style: TextStyle(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                  fontSize: 20),
              decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2]),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![0]),
                  ),
                  hintText:
                      'تومان${Get.find<controllerphoneinfo>(tag: 'secend').Chargemin.value}حداقل شارژ',
                  hintStyle: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                      fontSize: 16)),
            ),
            InkWell(
              onTap: () async {
                if (int.parse(maxcapsole.text) > int.parse(mincapsole.text) &&
                    int.parse(mincapsole.text) >= 1000 &&
                    int.parse(maxcapsole.text) >= 5000) {
                  Get.find<controllerphoneinfo>(tag: 'secend').Chargemax.value =
                      int.parse(maxcapsole.text);
                  Get.find<controllerphoneinfo>(tag: 'secend').Chargemin.value =
                      int.parse(mincapsole.text);
                  Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
                  Get.snackbar('اطلاعیه', 'تغییر مورد نظر انجام شد');
                } else {
                  Get.snackbar('خطا', 'اطلاعات درست و هر دو فیلد پر شود');
                }
              },
              child: Container(
                decoration: BoxDecoration(
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![0],
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                child: Text(
                  'تایید',
                  style: TextStyle(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2]),
                ),
              ),
            ),
          ],
        ),
      ),
    );

delete_device() => Get.defaultDialog(
    backgroundColor:
        theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
    title: 'هشدار',
    titleStyle: TextStyle(
        color:
            theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
        fontWeight: FontWeight.bold,
        fontSize: 16),
    middleTextStyle: TextStyle(
        color:
            theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
        fontWeight: FontWeight.bold,
        fontSize: 16),
    middleText: 'از حذف دستگاه مطمعن هستید؟',
    /*  onCancel: () => Get.,
                                                textCancel: 'خیر', */
    cancelTextColor:
        theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
    confirmTextColor:
        theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![2],
    buttonColor:
        theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
    textConfirm: 'بله',
    onConfirm: () {
      Get.find<controllerphoneinfo>(tag: 'secend').Deletephons();
      Get.back();
    });
