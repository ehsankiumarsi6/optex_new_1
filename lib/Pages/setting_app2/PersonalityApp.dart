// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Getxcontroller/controllershow.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_app2/PasswoedApp.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';

class PersonalityApp extends StatefulWidget {
  const PersonalityApp({Key? key}) : super(key: key);

  @override
  State<PersonalityApp> createState() => _PersonalityAppState();
}

class _PersonalityAppState extends State<PersonalityApp> {
  List<String> name1show = [
    'سایلنت',
    'نیمه فعال',
    'شنود',
    'برق شهر',
    'بلندگو',
    'تعداد ریموت',
    'آنتن',
    'تعداد مخاطب',
    'وضعیت شبکه ',
    'ولتاژ باطری',
    'وضعیت رله 1',
    'زون1',
    'زون2',
    'زون3',
    'زون4',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
          child: SingleChildScrollView(
              child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "شخصی سازی نرم افزار(نمایش آیتم ها)",
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
          ),
          Column(
            children: List.generate(
                name1show.length,
                (index) => Column(
                      children: [
                        Box_All_1(
                            width: Get.width * 0.9,
                            hight: 20,
                            widget: Container(),
                            text1: ''),
                        Obx(() {
                          return CheckboxListTile(
                            value: Get.find<controllershow>(tag: 'secend')
                                .showf[index]
                                .value,
                            onChanged: (value) {
                              Get.find<controllershow>(tag: 'secend')
                                  .changeshow(
                                      index, name1show[index], value ?? true);
                            },
                            title: Text(
                              name1show[index],
                              style: TextStyle(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![2],
                              ),
                            ),
                          );
                        }),
                      ],
                    )),
          )
        ],
      ))),
    );
  }
}
