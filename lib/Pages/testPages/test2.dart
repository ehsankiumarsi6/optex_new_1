import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import '/Getxcontroller/controllerOnOff.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/contacts/contacts.dart';
import '/Pages/homepage/HomePage.dart';
import '/Pages/homepage/widget.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import '../../Color.dart';
import 'package:permission_handler/permission_handler.dart';

class test2 extends StatefulWidget {
  const test2({super.key});

  @override
  State<test2> createState() => _test2State();
}

class _test2State extends State<test2> {
  late stt.SpeechToText _speech;
  bool _isListening = false;
  late PermissionStatus _microphonePermissionStatus;
  String _text = '';
  late final FlutterTts flutterTts1;

  @override
  void initState() {
    _speech = stt.SpeechToText();
    flutterTts1 = FlutterTts();
    //  checkMicrophonePermission();
    // TODO: implement initState
    super.initState();
  }

  Listen_name2() async {
    /*  final player2 = AudioPlayer();
    await player2.setAsset('music/name_device.mp3');
    player2.play(); */
    //player2.dispose();
    print('listten');
    if (!_isListening) {
      bool available = await _speech.initialize(
          /*  onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'), */
          );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              /* Get.find<controllerphoneinfo>(tag: 'secend').textnamephone.text =
                  _text; */
              // Listen_phone();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  Listen_phone() async {
    final player3 = AudioPlayer();
    await player3.setAsset('music/phone_device.mp3');
    player3.play();

    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() async {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              Get.find<controllerphoneinfo>(tag: 'secend').textphone.text =
                  _text;
              final player3 = AudioPlayer();
              await player3.setAsset('music/doit_create.mp3');
              player3.play();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.access_alarm_rounded),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                onPressed: () {},
                child: Icon(Icons.access_alarm_rounded),
                backgroundColor: const Color.fromARGB(255, 117, 100, 50),
              ),
            ),
            MaterialButton(
              onPressed: () => ehsan(),
              child: Text('ehsan'),
            ),
            SizedBox(
              height: 20,
            ),
            MaterialButton(
              onPressed: () => Listen_name2(),
              child: Text('ehsan2'),
            ),
            SizedBox(
              height: 30,
            ),
            Text(_text),
          ],
        ),
      ),
    );
  }

  void ehsan() async {
    final player = AudioPlayer();
    await player.setAsset('music/name_device.mp3');
    await player.play();

    player.playerStateStream.listen((playerState) {
      if (playerState.processingState == ProcessingState.completed) {
        Listen_name2();
      }
    });
  }
}

Future<void> ehsan2() async {
  await Future.delayed(
    Duration(seconds: 5),
    () => print('2'),
  );
}
