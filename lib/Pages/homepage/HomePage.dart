// ignore_for_file: camel_case_types, file_names
//   keytool -genkey -v -keystore C:\Users\ehsan\optex_2.jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias upload

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:new_test_3/Getxcontroller/controllerOnOff.dart';
import 'package:new_test_3/Pages/add_devices/add_devices.dart';
import 'package:new_test_3/funtion_al.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/darwer/drawer.dart';
import '/Pages/homepage/widget.dart';

import '../../Color.dart';
import 'package:card_swiper/card_swiper.dart';

class homepage extends StatefulWidget {
  const homepage({Key? key}) : super(key: key);

  @override
  State<homepage> createState() => _homepageState();
}

class _homepageState extends State<homepage> {
  @override
  void initState() {
    ifgotoadddevice();
    // TODO: implement initState
    super.initState();
  }

  ifgotoadddevice() async {
    await Future.delayed(Duration(seconds: 1));
    if (Get.find<controllerphoneinfo>(tag: 'secend').lenghtmainpage.value ==
        0) {
      Get.off(AddDevices());
    }
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey =
        new GlobalKey<ScaffoldState>();

    return SafeArea(
      child: Obx(() {
        return Scaffold(
          // bottomSheet: Container(child: DraggableScrollableSheet(bui)),
          key: _scaffoldKey,
          endDrawer: drawer(),
          floatingActionButton: FloatingActionButton(
            backgroundColor: theme[
                Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            onPressed: () {
              listen_homepage();
            },
            child: Icon(Icons.record_voice_over_rounded),
          ),
          body: Stack(
            children: [
              Container(
                height: Get.height,
                width: Get.width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  //begin: Alignment.topLeft,
                  end: FractionalOffset.bottomRight,
                  //  stops: [_animation1.value, 0.5],
                  colors: [
                    /* Colors.red,
                      Colors.blue */
                    // Theme(data: data, child: child)
                    theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![1],
                    theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ],
                )),
                child: Center(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Container(
                              height: Get.height * 0.07,
                              child: Image.asset(
                                  Get.find<controllerphoneinfo>(tag: 'secend')
                                              .theme
                                              .value !=
                                          'red'
                                      ? 'images/logo_blue.png'
                                      : 'images/logo_red.png'),
                            ),
                          ),
                          Obx(() {
                            return Text(
                              Get.find<controlleronoff>(tag: 'secend')
                                  .text_listen
                                  .value,
                              style: TextStyle(
                                  color:
                                      Get.find<controlleronoff>(tag: 'secend')
                                                  .green_listen
                                                  .value ==
                                              true
                                          ? Colors.green
                                          : Colors.red,
                                  fontSize: 17),
                            );
                          }),
                          InkWell(
                              onTap: () =>
                                  _scaffoldKey.currentState!.openEndDrawer(),
                              child: Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Icon(
                                  Icons.settings,
                                  size: 30,
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![2],
                                ),
                              ))
                        ],
                      ),
                      Center(
                        child: Transform.translate(
                          offset: Offset(0, Get.height * 0.055),
                          child: Container(
                              height: 500,
                              // padding: const EdgeInsets.only(left: 32),
                              child: Obx(() {
                                return Swiper(
                                    onIndexChanged: (index) {
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .changepage(index);
                                      Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .lenghtmainpage ==
                                              1
                                          ? Get.snackbar('توجه',
                                              'شما فقط همین یک دستگاه را دارید تغییر دستگاه صورت نگرفت')
                                          : null;
                                    },
                                    //loop: false,
                                    axisDirection: AxisDirection.right,
                                    itemCount: Get.find<controllerphoneinfo>(
                                                    tag: 'secend')
                                                .lenghtmainpage
                                                .value ==
                                            0
                                        ? 1
                                        : Get.find<controllerphoneinfo>(
                                                tag: 'secend')
                                            .lenghtmainpage
                                            .value,
                                    itemWidth:
                                        MediaQuery.of(context).size.width -
                                            2 * 50,
                                    layout: SwiperLayout.STACK,
                                    itemBuilder: (context, index) {
                                      return Container(
                                        child: controlle_onoff(),
                                        // color: colors[index],
                                      );
                                    });
                              })),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              bottom_sheet()
            ],
          ),
        );
      }),
    );
  }
}
