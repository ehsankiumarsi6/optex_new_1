import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/relleh/widget_relleh.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';

class Relleh extends StatefulWidget {
  const Relleh({Key? key}) : super(key: key);

  @override
  State<Relleh> createState() => _RellehState();
}

class _RellehState extends State<Relleh> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
          child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'رله',
                  style: textstyle_inqury,
                ),
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.help,
                  color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                      .theme
                      .value]![2],
                )
              ],
            ),
          ),
          SizedBox(height: Get.height * 0.07),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50),
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                Get.find<controllerphoneinfo>(tag: 'secend').Relehs[0] == ' '
                    ? 'رله 1'
                    : Get.find<controllerphoneinfo>(tag: 'secend').Relehs[0],
                style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          SizedBox(height: Get.height * 0.03),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [releh1_on(), releh1_open(), releh1_off()],
          ),
          SizedBox(height: Get.height * 0.03),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50),
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                Get.find<controllerphoneinfo>(tag: 'secend').Relehs[2] == ' '
                    ? 'رله 2'
                    : Get.find<controllerphoneinfo>(tag: 'secend').Relehs[2],
                style: TextStyle(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          SizedBox(height: Get.height * 0.03),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [releh2_on(), releh2_open(), releh2_off()],
          ),
          SizedBox(height: Get.height * 0.03),
          InkWell(
            onTap: () => timerelleh1(),
            child: Column(children: [
              Box_All_1(
                  width: Get.width * 0.9,
                  hight: 25,
                  widget: Container(),
                  text1: ''),
              Obx(() {
                return Text('زمان درب بازکن 1',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17));
              }),
            ]),
          ),
          InkWell(
            onTap: () => timerelleh2(),
            child: Column(children: [
              Box_All_1(
                  width: Get.width * 0.9,
                  hight: 25,
                  widget: Container(),
                  text1: ''),
              Obx(() {
                return Text('زمان درب بازکن 2',
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17));
              }),
            ]),
          ),
          InkWell(
            onTap: () => namerelleh(),
            child: Column(children: [
              Box_All_1(
                  width: Get.width * 0.9,
                  hight: 25,
                  widget: Container(),
                  text1: ''),
              Obx(() {
                return Text("نام گذاری رله ها",
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17));
              }),
            ]),
          ),
          InkWell(
            onTap: () => settingrelleh(),
            child: Column(children: [
              Box_All_1(
                  width: Get.width * 0.9,
                  hight: 25,
                  widget: Container(),
                  text1: ''),
              Obx(() {
                return Text("تنظیمات بیشتر",
                    style: TextStyle(
                        color: theme[
                            Get.find<controllerphoneinfo>(tag: 'secend')
                                .theme
                                .value]![2],
                        fontSize: 17));
              }),
            ]),
          ),
        ],
      )),
    );
  }
}
