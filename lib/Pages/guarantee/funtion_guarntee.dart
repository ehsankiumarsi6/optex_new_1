import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

int copy_difference = 0;
calculateDays2(String text) {
  final now = DateTime.now();
  final selectedDate = DateFormat.yMd().parse(text);
  final difference = selectedDate.difference(now).inDays;
  copy_difference = difference;
  if (difference == 0) {
    return 'گارانتی فقط امروز است!!!!';
  } else if (difference > 0) {
    return 'گارانتی شما تا $difference روز دیگر فعال است';
  } else {
    return 'متاسفانه گارانتی ${difference - 1}روز است که رد شده است';
  }
}

MiladiToShamsi(int iMiladiMonth, int iMiladiDay, int iMiladiYear) {
  int shamsiDay, shamsiMonth, shamsiYear;
  int dayCount, farvardinDayDiff, deyDayDiff;
  List<int> sumDayMiladiMonth = [
    0,
    31,
    59,
    90,
    120,
    151,
    181,
    212,
    243,
    273,
    304,
    334
  ];
  List<int> sumDayMiladiMonthLeap = [
    0,
    31,
    60,
    91,
    121,
    152,
    182,
    213,
    244,
    274,
    305,
    335
  ];
  // SHAMSIDATE shamsidate;

  farvardinDayDiff = 79;

  if (MiladiIsLeap(iMiladiYear)) {
    dayCount = sumDayMiladiMonthLeap[iMiladiMonth - 1] + iMiladiDay;
  } else {
    dayCount = sumDayMiladiMonth[iMiladiMonth - 1] + iMiladiDay;
  }
  if ((MiladiIsLeap(iMiladiYear - 1))) {
    deyDayDiff = 11;
  } else {
    deyDayDiff = 10;
  }
  if (dayCount > farvardinDayDiff) {
    dayCount = dayCount - farvardinDayDiff;
    if (dayCount <= 186) {
      switch (dayCount % 31) {
        case 0:
          shamsiMonth = (dayCount / 31).toInt();
          shamsiDay = 31;
          break;
        default:
          shamsiMonth = ((dayCount / 31).toInt() + 1);
          shamsiDay = (dayCount % 31);
          break;
      }
      shamsiYear = iMiladiYear - 621;
    } else {
      dayCount = dayCount - 186;
      switch (dayCount % 30) {
        case 0:
          shamsiMonth = ((dayCount / 30).toInt() + 6);
          shamsiDay = 30;
          break;
        default:
          shamsiMonth = ((dayCount / 30).toInt() + 7);
          shamsiDay = (dayCount % 30);
          break;
      }
      shamsiYear = iMiladiYear - 621;
    }
  } else {
    dayCount = dayCount + deyDayDiff;

    switch (dayCount % 30) {
      case 0:
        shamsiMonth = ((dayCount / 30).toInt() + 9);
        shamsiDay = 30;
        break;
      default:
        shamsiMonth = ((dayCount / 30).toInt() + 10);
        shamsiDay = (dayCount % 30);
        break;
    }
    shamsiYear = iMiladiYear - 622;
  }

  return '$shamsiDay/$shamsiMonth/$shamsiYear';
  /*  iYear = shamsiYear;
  iMonth = shamsiMonth;
  iDay = shamsiDay;
  print('$shamsiDay/$shamsiMonth/$shamsiYear');
  Get.find<controllerDatabase>(tag: 'secend').shamsi =
      '$shamsiDay/$shamsiMonth/$shamsiYear | ${DateTime.now().hour}:${DateTime.now().minute}'
          .obs; */
}

// the function check a miladiyear is leap or not.
MiladiIsLeap(int miladiYear) {
  if (((miladiYear % 100) != 0 && (miladiYear % 4) == 0) ||
      ((miladiYear % 100) == 0 && (miladiYear % 400) == 0))
    return true;
  else
    return false;
}
