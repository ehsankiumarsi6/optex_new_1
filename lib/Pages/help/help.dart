// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/help/text_help.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';

class Help extends StatefulWidget {
  const Help({Key? key}) : super(key: key);

  @override
  State<Help> createState() => _HelpState();
}

class _HelpState extends State<Help> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:
            theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: Get.width * 0.4,
                      height: Get.width * 0.15,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![2],
                          ),
                          borderRadius: BorderRadius.circular(40)),
                      child: FittedBox(
                          child: Text(
                        'راهنمایی',
                        style: textstyle_inqury,
                      )),
                    ),
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'صفحه اصلی'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[0],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'افزودن دستگاه'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[1],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'تنظیم سیمکارت'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[2],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'تنظیمات'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[3],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'تنظیمات پیشرفته'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[4],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'مخاطبین'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[5],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'زون ها'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[6],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'تنظیمات نرم افزار اندروید'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[7],
                    style: textstyle_inqury,
                  ),
                ),
                Box_All(
                    width: Get.width * 0.9,
                    hight: 50,
                    widget: Container(),
                    text1: 'نیمه فعالسازی'),
                Container(
                  width: Get.width * 0.9,
                  margin: EdgeInsets.all(5),
                  child: Text(
                    helptext[8],
                    style: textstyle_inqury,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
