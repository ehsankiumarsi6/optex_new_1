import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import '/Getxcontroller/controllerOnOff.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/contacts/widget_contacts.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/testPages/test1.dart';
import 'package:telephony/telephony.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;

import '../../Color.dart';

class Contacts extends StatefulWidget {
  const Contacts({Key? key}) : super(key: key);

  @override
  State<Contacts> createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  late stt.SpeechToText _speech;
  bool _isListening = false;

  String _text = '';
  @override
  void initState() {
    _speech = _speech = stt.SpeechToText();
    // TODO: implement initState
     play_audio();
    super.initState();
  }

  play_audio() async {
    if (Get.find<controllerphoneinfo>(tag: 'secend').contact1[0] == ' ') {
      final player = AudioPlayer();
      await player.setAsset('music/name_contact.mp3');
      await player.play();
      player.playerStateStream.listen((event) {
        if (event.processingState == ProcessingState.completed) {
          player.dispose();
          // _listen2();
        }
      });
    }
  }

  void _listen2() async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              Get.find<controllerphoneinfo>(tag: 'secend').namecontect[0].text =
                  _text;
              sst2();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  void _listenName(i) async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              Get.find<controllerphoneinfo>(tag: 'secend').namecontect[i].text =
                  _text;
              //  sst2();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  void _listenLevel(i) async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              /* Get.find<controllerphoneinfo>(tag: 'secend').namecontect[i].text =
                  _text; */
              //  sst2();
              _text = _text.toEnglishDigit();
              if (_text == '1') {
                Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i] =
                    'A';
                print(
                    'Get.find<controllerphoneinfo>.dropdownValues[i]= ${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i]}');
              }
              if (_text == '2') {
                Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i] =
                    'B';
                print(
                    'Get.find<controllerphoneinfo>.dropdownValues[i]= ${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i]}');
              }
              if (_text == '3') {
                Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i] =
                    'C';
                print(
                    'Get.find<controllerphoneinfo>.dropdownValues[i]= ${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i]}');
              }
              if (_text == '4') {
                Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i] =
                    'D';
                print(
                    'Get.find<controllerphoneinfo>.dropdownValues[i]= ${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i]}');
              }
              Get.find<controllerphoneinfo>(tag: 'secend').contact1[i] =
                  '${Get.find<controllerphoneinfo>(tag: 'secend').namecontect[i].text}+${Get.find<controllerphoneinfo>(tag: 'secend').phonecontect[i].text}+${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i]}';

              Get.find<controllerphoneinfo>(tag: 'secend').updatePhone();
              final Telephony telephony = Telephony.instance;
              telephony.sendSms(
                to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                message:
                    '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*31*${i + 1}*${Get.find<controllerphoneinfo>(tag: 'secend').phonecontect[i].text}${Get.find<controllerphoneinfo>(tag: 'secend').dropdownValues[i]}#',
              );
              //     Get.back();
              Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
              Get.find<controllerphoneinfo>(tag: 'secend')
                  .startfifteensecends();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  void _listenPhone(i) async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              _text = _text.toEnglishDigit().replaceAll(' ', '');
              Get.find<controllerphoneinfo>(tag: 'secend')
                  .phonecontect[i]
                  .text = _text;
              // sst2();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  sst2() async {
    for (var i = 0; i < 10; i++) {
      if (Get.find<controllerphoneinfo>(tag: 'secend').contact1[i] == ' ') {
        if (Get.find<controllerphoneinfo>(tag: 'secend').namecontect[i].text ==
            '') {
          final player_A = AudioPlayer();
          await player_A.setAsset('music/name_contact.mp3');
          player_A.play();
          player_A.playerStateStream.listen((event) {
            if (event.processingState == ProcessingState.completed) {
              player_A.dispose();
              _listenName(i);
            }
          });
        } else {
          if (Get.find<controllerphoneinfo>(tag: 'secend')
                  .phonecontect[i]
                  .text ==
              '') {
            final player_A = AudioPlayer();
            await player_A.setAsset('music/phone_contact.mp3');
            await player_A.play();
            player_A.playerStateStream.listen((event) {
              if (event.processingState == ProcessingState.completed) {
                _listenPhone(i);
                player_A.dispose();
              }
            });
          } else {
            final player_A = AudioPlayer();
            await player_A.setAsset('music/level_avelible.mp3');
            await player_A.play();
            player_A.playerStateStream.listen((event) {
              if (event.processingState == ProcessingState.completed) {
                player_A.dispose();
              }
            });
          }
        }
        break;
      }
    }
  }

  speak_agent() async {
    for (var i = 0; i < 10; i++) {
      if (Get.find<controllerphoneinfo>(tag: 'secend').contact1[i] == ' ') {
        if (Get.find<controllerphoneinfo>(tag: 'secend').namecontect[i].text ==
            '') {
          final player_A = AudioPlayer();
          await player_A.setAsset('music/name_contact.mp3');
          player_A.play();
          player_A.playerStateStream.listen((event) {
            if (event.processingState == ProcessingState.completed) {
              player_A.dispose();
              //  _listenName(i);
            }
          });
        } else {
          if (Get.find<controllerphoneinfo>(tag: 'secend')
                  .phonecontect[i]
                  .text ==
              '') {
            final player_A = AudioPlayer();
            await player_A.setAsset('music/phone_contact.mp3');
            await player_A.play();
            player_A.playerStateStream.listen((event) {
              if (event.processingState == ProcessingState.completed) {
                //  _listenPhone(i);
                player_A.dispose();
              }
            });
          } else {
            final player_A = AudioPlayer();
            await player_A.setAsset('music/level_avelible.mp3');
            await player_A.play();
            player_A.playerStateStream.listen((event) {
              if (event.processingState == ProcessingState.completed) {
                player_A.dispose();
              }
            });
          }
        }
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      /* floatingActionButton: FloatingActionButton(
        onPressed: sst2,
        child: Icon(Icons.record_voice_over_rounded),
        backgroundColor:
            theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
      ), */
      extendBody: true,
      bottomNavigationBar: Container(
        width: Get.width * 0.9,
        margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FloatingActionButton(
              onPressed: () {
                for (var i = 0; i < 10; i++) {
                  if (Get.find<controllerphoneinfo>(tag: 'secend')
                          .contact1[i] ==
                      ' ') {
                    if (Get.find<controllerphoneinfo>(tag: 'secend')
                            .namecontect[i]
                            .text ==
                        '') {
                      _listenName(i);
                    } else {
                      if (Get.find<controllerphoneinfo>(tag: 'secend')
                              .phonecontect[i]
                              .text ==
                          '') {
                        _listenPhone(i);
                      } else {
                        Get.snackbar('توجه',
                            'توجه برای راحتی کاربران بخاطر مشکل تلفظ انگلیسی \n میتوانید از سطح یک تا چهار را به منشی فرمانپذیر بگویید \n a = یک b = دو c =سه d = چهار',
                            //  'برای راحتی کاربران با مشکل تلفظ کلمات انگلیسی برای فرمان دادن به جای A,B,C,Dمیتوانید از 1و2و3و4واستفاده کنید',
                            duration: Duration(seconds: 10));
                        _listenLevel(i);
                      }
                    }
                    break;
                  }
                }
              },
              child: Icon(Icons.record_voice_over_rounded),
              tooltip: 'شنیدن',
              backgroundColor: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            ),
            Text(
              _text,
              style: textstyle_inqury,
            ),
            FloatingActionButton(
              onPressed: () => speak_agent(),
              child: Icon(Icons.support_agent),
              tooltip: 'منشی سخنگو',
              backgroundColor: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(3.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'مخاطبین',
                      style: textstyle_inqury,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.help,
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![2],
                    )
                  ],
                ),
              ),
              Obx(() {
                return Visibility(
                  visible: Get.find<controlleronoff>(tag: 'secend')
                      .Inquirycontect
                      .value,
                  child: CircularProgressIndicator(
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                  replacement: Obx(() {
                    return InkWell(
                      onTap: Get.find<controllerphoneinfo>(tag: 'secend')
                                  .fifteensecends
                                  .value >=
                              15
                          ? () async {
                              Get.defaultDialog(
                                  backgroundColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![1],
                                  title: 'هشدار',
                                  titleStyle: textstyle_inqury,
                                  middleTextStyle: textstyle_inqury,
                                  middleText: 'پیامک فرستاده شود؟',
                                  confirmTextColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![2],
                                  buttonColor: theme[
                                      Get.find<controllerphoneinfo>(
                                              tag: 'secend')
                                          .theme
                                          .value]![0],
                                  textConfirm: 'بله',
                                  onConfirm: () {
                                    final Telephony telephony =
                                        Telephony.instance;
                                    //  String copyMassege = '0,1,1110,0,1,30,1,1,99,0';
                                    telephony.sendSms(
                                      to: "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}",
                                      message:
                                          //'*09307112409B1#',
                                          '*${Get.find<controllerphoneinfo>(tag: 'secend').Password.value}*91#',
                                    );
                                    Get.back();
                                    Get.find<controlleronoff>(tag: 'secend')
                                        .Inquirycontect
                                        .value = true;

                                    Get.snackbar('اطلاعیه', 'پیامک فرستاده شد');
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .startfifteensecends();
                                    telephony.listenIncomingSms(
                                        onNewMessage: (SmsMessage message) {
                                          // Handle message
                                          if (message.address ==
                                                  "+98${Get.find<controllerphoneinfo>(tag: 'secend').phone.value}" &&
                                              message.body!.split('*0').length >
                                                  1) {
                                            Get.find<controlleronoff>(
                                                    tag: 'secend')
                                                .Inquirycontect
                                                .value = false;
                                            Get.snackbar(
                                                'اطلاعیه', 'پیامک دریافت شد');
                                            List<String> copycontexts =
                                                message.body!.split('*');
                                            for (var i = 1;
                                                i < copycontexts.length;
                                                i++) {
                                              String copyphonecontext = '';
                                              for (var j = 0; j < 11; j++) {
                                                copyphonecontext =
                                                    copyphonecontext +
                                                        copycontexts[i][j];
                                              }
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .phonecontect[i - 1]
                                                  .text = copyphonecontext;
                                              Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .dropdownValues[i - 1] =
                                                  copycontexts[i][11];
                                              Get.find<controllerphoneinfo>(
                                                          tag: 'secend')
                                                      .contact1[i - 1] =
                                                  ' +${copyphonecontext}+${copycontexts[i][11]}';
                                              Get.find<controllerphoneinfo>(
                                                      tag: 'secend')
                                                  .updatePhone();
                                            }
                                          }
                                        },
                                        listenInBackground: false);
                                  });
                            }
                          : () => Get.snackbar(
                              'خطا', 'هر 15 ثانیه یکبار درخواست ارسال کنید'),
                      child: Container(
                        width: Get.width * 0.9,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: theme[
                                Get.find<controllerphoneinfo>(tag: 'secend')
                                    .theme
                                    .value]![0],
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: Obx(() {
                            return Text(
                              'استعلام مخاطب',
                              style: TextStyle(
                                  color: theme[Get.find<controllerphoneinfo>(
                                          tag: 'secend')
                                      .theme
                                      .value]![2]),
                            );
                          }),
                        ),
                      ),
                    );
                  }),
                );
              }),
              Column(
                children:
                    List.generate(8, (index) => contacts_widget(index: index)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
