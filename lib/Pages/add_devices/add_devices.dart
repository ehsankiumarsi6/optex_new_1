//import 'package:audioplayers/audioplayers.dart';
//import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:get/get.dart';
import 'package:just_audio/just_audio.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import '/Getxcontroller/controllerOnOff.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/contacts/contacts.dart';
import '/Pages/homepage/HomePage.dart';
import '/Pages/homepage/widget.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import '../../Color.dart';
import 'package:permission_handler/permission_handler.dart';
//import 'package:assets_audio_player/assets_audio_player.dart';
//import 'package:assets_audio_player/assets_audio_player.dart';
//import 'package:just_audio/just_audio.dart';

class AddDevices extends StatefulWidget {
  const AddDevices({Key? key}) : super(key: key);

  @override
  State<AddDevices> createState() => _AddDevicesState();
}

class _AddDevicesState extends State<AddDevices> {
  late stt.SpeechToText _speech;
  bool _isListening = false;
  late PermissionStatus _microphonePermissionStatus;
  late PermissionStatus _permissionStatus;
  String _text = '';
  late final FlutterTts flutterTts1;
  @override
  void initState() {
    _speech = stt.SpeechToText();
    checkMicrophonePermission();

    flutterTts1 = FlutterTts();

    audio_seturi_play();
    // Listen_name();
    // TODO: implement initState
    super.initState();
  }

  Future<void> checkPermissionStatus() async {
    final status = await Permission.sms.status;
    setState(() {
      _permissionStatus = status;
    });
    if (!_permissionStatus.isGranted) {
      requestPermission();
    }
  }

  Future<void> requestPermission() async {
    final status = await Permission.sms.request();
    setState(() {
      _permissionStatus = status;
    });
  }

  Future<void> checkMicrophonePermission() async {
    PermissionStatus status = await Permission.microphone.status;
    setState(() {
      _microphonePermissionStatus = status;
    });

    if (!_microphonePermissionStatus.isGranted) {
      requestMicrophonePermission();
    }
  }

  Future<void> requestMicrophonePermission() async {
    PermissionStatus status = await Permission.microphone.request();
    setState(() {
      _microphonePermissionStatus = status;
    });
  }

  Listen_name() async {
    /*  final player2 = AudioPlayer();
    await player2.setAsset('music/name_device.mp3');
    player2.play(); */
    //player2.dispose();
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              Get.find<controllerphoneinfo>(tag: 'secend').textnamephone.text =
                  _text;
              Listen_phone();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  Listen_phone() async {
    final player3 = AudioPlayer();
    await player3.setAsset('music/phone_device.mp3');
    player3.play();

    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() async {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              Get.find<controllerphoneinfo>(tag: 'secend').textphone.text =
                  _text;
              final player3 = AudioPlayer();
              await player3.setAsset('music/doit_create.mp3');
              player3.play();
              print(_text);
            }
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  void audio_seturi_play() async {
    if (Get.find<controllerphoneinfo>(tag: 'secend').lenghtmainpage.value ==
        0) {
      final player = AudioPlayer();
      await player.setAsset('music/welcome_optex.mp3');
      await player.play();
      sst();
      // Listen_name();
    }
  }

  void _listen() async {
    if (!_isListening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        //۱۲۳۴۵۶۷۸۹۱۰
        //  _speech.systemLocale() ;
        setState(() => _isListening = true);
        _speech.listen(
          localeId: 'fa_IR',
          onResult: (val) => setState(() {
            _text = val.recognizedWords;
            if (val.hasConfidenceRating && val.confidence > 0) {
              _text = val.recognizedWords;
              if (Get.find<controllerphoneinfo>(tag: 'secend')
                      .textnamephone
                      .text ==
                  '') {
                Get.find<controllerphoneinfo>(tag: 'secend')
                    .textnamephone
                    .text = _text;
              } else {
                _text = _text.toEnglishDigit().replaceAll(' ', '');
                Get.find<controllerphoneinfo>(tag: 'secend').textphone.text =
                    _text;
                for (var element in ha) {
                  if (element ==
                      '${_text[0]}${_text[1]}${_text[2]}${_text[3]}') {
                    Get.find<controlleronoff>(tag: 'secend').opretorsim.value =
                        'ha';
                  }
                }
                for (var element in ir) {
                  if (element ==
                      '${_text[0]}${_text[1]}${_text[2]}${_text[3]}') {
                    Get.find<controlleronoff>(tag: 'secend').opretorsim.value =
                        'ir';
                  }
                }
                for (var element in rl) {
                  if (element ==
                      '${_text[0]}${_text[1]}${_text[2]}${_text[3]}') {
                    Get.find<controlleronoff>(tag: 'secend').opretorsim.value =
                        'rl';
                  }
                }
              }
              print(_text);
            }
            //  sst();
          }),
        );

        // تنظیم زبان به فارسی
      }
    } else {
      setState(() => _isListening = false);
      _speech.stop();
    }
  }

  sst() async {
    if (Get.find<controllerphoneinfo>(tag: 'secend').textnamephone.text == '') {
      final player3 = AudioPlayer();
      await player3.setAsset('music/name_device.mp3');
      await player3.play();
      player3.playerStateStream.listen((playerState) {
        if (playerState.processingState == ProcessingState.completed) {
          //  _listen();
        }
      });
      print('namephone');

      print('namephone2');
    } else {
      if (Get.find<controllerphoneinfo>(tag: 'secend').textphone.text == '') {
        final player4 = AudioPlayer();
        await player4.setAsset('music/phone_device.mp3');
        await player4.play();
        player4.playerStateStream.listen((playerState) {
          if (playerState.processingState == ProcessingState.completed) {
            //  _listen();
          }
        });
        print('yes');
      } else {
        final player5 = AudioPlayer();
        await player5.setAsset('music/doit_create.mp3');
        player5.play();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      extendBody: true,
      bottomNavigationBar: Container(
        width: Get.width * 0.9,
        margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FloatingActionButton(
              onPressed: _listen
              //  sst();
              ,
              child: Icon(Icons.record_voice_over_rounded),
              tooltip: 'شنیدن',
              backgroundColor: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            ),
            Text(
              _text,
              style: textstyle_inqury,
            ),
            FloatingActionButton(
              onPressed: sst,
              child: Icon(Icons.support_agent),
              tooltip: 'منشی سخنگو',
              backgroundColor: theme[
                  Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![0],
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'افزودن دستکاه',
                    style: textstyle_inqury,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.help,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 100,
            ),
            Container(
              width: Get.width * 0.7,
              height: Get.height * 0.1,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                      width: 3.5),
                  borderRadius: BorderRadius.circular(40)),
              child: Center(
                child: TextField(
                  style: textstyle_inqury,
                  controller: Get.find<controllerphoneinfo>(tag: 'secend')
                      .textnamephone,
                  decoration: InputDecoration(
                    hintText: 'نام دستگاه',
                    hintStyle: textstyle_inqury,
                    // alignLabelWithHint: true
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: Get.width * 0.7,
              height: Get.height * 0.1,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                          .theme
                          .value]![0],
                      width: 3.5),
                  borderRadius: BorderRadius.circular(40)),
              child: Center(
                child: TextField(
                  style: textstyle_inqury,
                  keyboardType: TextInputType.phone,
                  controller:
                      Get.find<controllerphoneinfo>(tag: 'secend').textphone,
                  onChanged: (text) {
                    if (text.length >= 4) {
                      for (var element in ha) {
                        if (element ==
                            '${text[0]}${text[1]}${text[2]}${text[3]}') {
                          Get.find<controlleronoff>(tag: 'secend')
                              .opretorsim
                              .value = 'ha';
                        }
                      }
                      for (var element in ir) {
                        if (element ==
                            '${text[0]}${text[1]}${text[2]}${text[3]}') {
                          Get.find<controlleronoff>(tag: 'secend')
                              .opretorsim
                              .value = 'ir';
                        }
                      }
                      for (var element in rl) {
                        if (element ==
                            '${text[0]}${text[1]}${text[2]}${text[3]}') {
                          Get.find<controlleronoff>(tag: 'secend')
                              .opretorsim
                              .value = 'rl';
                        }
                      }
                    }
                  },
                  decoration: InputDecoration(
                    hintText: '099..شماره تلفن دستگاه',
                    hintStyle: textstyle_inqury,
                    // alignLabelWithHint: true
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: Get.width * 0.65,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () => Get.find<controlleronoff>(tag: 'secend')
                        .opretorsim
                        .value = 'ha',
                    child: Obx(() {
                      return Container(
                        width: Get.width * 0.17,
                        padding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Get.find<controlleronoff>(tag: 'secend')
                                        .opretorsim
                                        .value ==
                                    'ha'
                                ? Colors.lightBlue
                                : null,
                            border: Border.all(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0],
                                width: 1.7),
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: FittedBox(
                              child: Text(
                            'همراه اول',
                            style: textstyle_inqury,
                            textAlign: TextAlign.center,
                          )),
                        ),
                      );
                    }),
                  ),
                  InkWell(
                    onTap: () => Get.find<controlleronoff>(tag: 'secend')
                        .opretorsim
                        .value = 'rl',
                    child: Obx(() {
                      return Container(
                        width: Get.width * 0.17,
                        padding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Get.find<controlleronoff>(tag: 'secend')
                                        .opretorsim
                                        .value ==
                                    'rl'
                                ? Colors.purple[100]
                                : null,
                            border: Border.all(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0],
                                width: 1.7),
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: FittedBox(
                              child: Text(
                            'رایتل',
                            style: textstyle_inqury,
                            textAlign: TextAlign.center,
                          )),
                        ),
                      );
                    }),
                  ),
                  InkWell(
                    onTap: () => Get.find<controlleronoff>(tag: 'secend')
                        .opretorsim
                        .value = 'ir',
                    child: Obx(() {
                      return Container(
                        width: Get.width * 0.17,
                        padding:
                            EdgeInsets.symmetric(vertical: 7, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Get.find<controlleronoff>(tag: 'secend')
                                        .opretorsim
                                        .value ==
                                    'ir'
                                ? Colors.yellow[400]
                                : null,
                            border: Border.all(
                                color: theme[
                                    Get.find<controllerphoneinfo>(tag: 'secend')
                                        .theme
                                        .value]![0],
                                width: 1.7),
                            borderRadius: BorderRadius.circular(40)),
                        child: Center(
                          child: FittedBox(
                              child: Text(
                            'ایرانسل',
                            style: textstyle_inqury,
                            textAlign: TextAlign.center,
                          )),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                Get.defaultDialog(
                    backgroundColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![1],
                    title: 'هشدار',
                    titleStyle: textstyle_inqury,
                    middleTextStyle: textstyle_inqury,
                    middleText: 'از ساخت دستگاه مطمعن هستید؟',
                    /*  onCancel: () => Get.,
                                        textCancel: 'خیر', */
                    cancelTextColor: Get.find<controlleronoff>(tag: 'secend')
                            .Themecolor[
                        Get.find<controlleronoff>(tag: 'secend').moodcolor]![2],
                    confirmTextColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![2],
                    buttonColor: theme[
                        Get.find<controllerphoneinfo>(tag: 'secend')
                            .theme
                            .value]![0],
                    textConfirm: 'بله',
                    onConfirm: () async {
                      Get.back();
                      Get.find<controllerphoneinfo>(tag: 'secend').adddevice();
                      checkPermissionStatus();
                      //Get.back();
                      Get.off(Contacts());
                      await Future.delayed(Duration(seconds: 2));
                      Get.snackbar('توجه', 'گارانتی شما از امروز فعال شد');
                    });
              },
              child: Container(
                  width: Get.width * 0.3,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![0],
                  ),
                  child: Center(
                      child: Text(
                    'ثبت دستگاه',
                    style: textstyle_inqury,
                  ))),
            )
          ],
        ),
      ),
    );
  }
}

List<String> ha = [
  '0910',
  '0911',
  '0912',
  '0913',
  '0914',
  '0915',
  '0916',
  '0917',
  '0918',
  '0919',
  '0990',
  '0991',
  '0992',
  '0993',
];

List<String> ir = [
  '0930',
  '0933',
  '0935',
  '0936',
  '0937',
  '0938',
  '0939',
  '0901',
  '0902',
  '0903',
  '0904',
  '0905',
];

List<String> rl = [
  '0920',
  '0921',
  '0922',
];
