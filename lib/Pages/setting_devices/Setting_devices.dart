// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/Getxcontroller/controllerphoneinfo.dart';
import '/Pages/homepage/widget.dart';
import '/Pages/setting_devices/widget_SettingDevices.dart';
import '/Pages/testPages/test1.dart';
import '/Widget/widget1.dart';

import '../../Color.dart';

class Setting_devices extends StatefulWidget {
  const Setting_devices({Key? key}) : super(key: key);

  @override
  State<Setting_devices> createState() => _Setting_devicesState();
}

// ignore: camel_case_types
class _Setting_devicesState extends State<Setting_devices> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          theme[Get.find<controllerphoneinfo>(tag: 'secend').theme.value]![1],
      body: SafeArea(
        child: SingleChildScrollView(
            child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'تنظیمات دستگاه',
                    style: textstyle_inqury,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.help,
                    color: theme[Get.find<controllerphoneinfo>(tag: 'secend')
                        .theme
                        .value]![2],
                  )
                ],
              ),
            ),
            Box_All(
                width: Get.width * 0.85,
                hight: Get.height * 0.20,
                widget: languge(),
                text1: 'زبان دستگاه'),
            Box_All(
                width: Get.width * 0.85,
                hight: Get.height * 0.20,
                widget: call_emergency(),
                text1: 'تماس در قطعی برق'),
            Box_All(
                width: Get.width * 0.85,
                hight: Get.height * 0.20,
                widget: time_alarm(),
                text1: 'زمان آژیر'),
            Box_All(
                width: Get.width * 0.85,
                hight: Get.height * 0.20,
                widget: mood_alarm(),
                text1: 'مد آلارم'),
            Box_All(
                width: Get.width * 0.85,
                hight: Get.height * 0.20,
                widget: inkiling_holding(),
                text1: 'گزارش دوره ای موجودی'),
            Box_All(
                width: Get.width * 0.85,
                hight: Get.height * 0.20,
                widget: inkiling_battery(),
                text1: 'گزارش دوره ای باتری'),
          ],
        )),
      ),
    );
  }
}
